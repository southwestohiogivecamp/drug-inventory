create table stock (
  stock_id bigserial primary key,
  packaging_id bigint not null references packaging,
  expiration_date date,
  created_date timestamptz not null default(now()),
  constraint UQ_STOCK_EXP unique (packaging_id, expiration_date)
);

-- create unique constraint for packaging_id, expiration_date?

create table stock_location (
  stock_location_id bigserial primary key,
  stock_id bigint not null references stock,
  location_id bigint not null references location,
  count integer not null default 0
);
