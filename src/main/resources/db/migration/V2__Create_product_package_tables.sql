create table product (
  product_id bigserial primary key,
  name varchar(255) not null,
  brand_name varchar(255),
  active_unit_strength varchar(255),
  dosage varchar(255),
  route varchar(255),
  pharmacy_classes varchar(255),
  dea_schedule varchar(255)
);

create table packaging (
  packaging_id bigserial primary key,
  description varchar(4000) not null,
  manufacturer varchar(255),
  wholesale_average_cost decimal(15,2),
  ndc char(11),
  upc char(12),
  product_id bigint references product
);
