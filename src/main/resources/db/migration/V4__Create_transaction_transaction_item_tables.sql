create table transaction (
  transaction_id bigserial primary key,
  description varchar(255) not null,
  source_inventory_id bigint not null references inventory(inventory_id),
  destination_inventory_id bigint not null references inventory(inventory_id),
  status varchar(10) not null default('PLANNING') 
    check (status in ('PLANNING', 'INPROCESS', 'COMPLETE')),
  created timestamptz not null default(now()),
  last_updated timestamptz not null default(now())
);

create table transaction_item (
  transaction_item_id bigserial primary key,
  transaction_id bigint not null references transaction,
  stock_id bigint not null references stock,
  source_location_id bigint not null references location(location_id),
  target_location_id bigint references location(location_id),
  status varchar(10) not null default('RESERVED')
    check (status in ('RESERVED','VERIFIED')),
  count integer not null default(0)
);
