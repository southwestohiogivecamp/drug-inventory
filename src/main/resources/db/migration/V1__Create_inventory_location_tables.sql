create table inventory (
  inventory_id serial primary key,
  inventory_type integer not null,
  name varchar(255) unique not null,
  description varchar(4000)
);

create table location (
  location_id bigserial primary key,
  name varchar(255) unique not null,
  description varchar(4000),
  active boolean not null default(true),
  inventory_id integer not null references inventory
);

