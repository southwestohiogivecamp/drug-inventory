/**
 * 
 */
function captureEnterAndJumpToField($captureField, $toField) {
	$captureField.on('keypress', function(e) {
		if(e.which == 13) {
			e.preventDefault();
			$toField.focus();
		}
	});
}