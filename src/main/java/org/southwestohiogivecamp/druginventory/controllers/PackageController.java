package org.southwestohiogivecamp.druginventory.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.southwestohiogivecamp.druginventory.domain.NdcCode;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.southwestohiogivecamp.druginventory.services.PackagingService;
import org.southwestohiogivecamp.druginventory.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/package")
public class PackageController {

	@Autowired
	private ProductService prodService;

	@Autowired
	private PackagingService pkgService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	private static final String PRODUCT_ID_SESSION = "productIdFilter";
	
	@GetMapping("/clear")
	public String clearFilter(HttpSession session) {
		session.removeAttribute(PRODUCT_ID_SESSION);
		return "redirect:/package";
	}

	@GetMapping
	public ModelAndView indexWithFilter(
			@RequestParam(required = false, value = "productIdFilter") Long productIdFilterParam, HttpSession session) {
		List<Packaging> packages = null;

		ModelAndView mav = new ModelAndView("packaging/packaging");

		Long productIdFilter = productIdFilterParam;
		if (productIdFilter == null || productIdFilter == 0) {
			Product filterProduct = (Product) session.getAttribute(PRODUCT_ID_SESSION);
			if (filterProduct != null) {
				packages = filterProduct.getPackages();
				mav.addObject("product", filterProduct);
			} else {
				packages = pkgService.findAll();
			}
		} else {
			Product product = prodService.findOne(productIdFilter);
			if (product != null) {
				packages = product.getPackages();
				mav.addObject("product", product);
			}
		}

		return mav.addObject("packagings", packages);
	}

	private ModelAndView packagingMav(Packaging p) {
		final List<Product> products = prodService.findAll();

		final ModelAndView mav = new ModelAndView("packaging/packagingForm").addObject("products", products)
				.addObject("packaging", p);

		return mav;
	}

	@GetMapping("/create")
	public ModelAndView createNew(@RequestParam(required = false, value = "productIdFilter") Long productIdFilter,
			HttpSession session) {
		Packaging pkg = new Packaging();
		if (productIdFilter != null && productIdFilter > 0) {
			Product product = prodService.findOne(productIdFilter);
			product.getPackages().size();
			session.setAttribute(PRODUCT_ID_SESSION, product);
			pkg.setProduct(product);
		} 
		return packagingMav(pkg);
	}

	@GetMapping("/{id}")
	public ModelAndView index(@PathVariable long id) {
		Packaging p = pkgService.findOne(id);

		if (p == null) {
			p = new Packaging();
		}

		return packagingMav(p);
	}

	@PostMapping("/{id}")
	public ModelAndView save(@PathVariable long id, @Valid Packaging packaging, BindingResult bindingResult) {
		packaging.setPackagingId(id);

		if (bindingResult.hasErrors()) {
			return packagingMav(packaging);
		}

		NdcCode ndc = NdcCode.codeFor(packaging.getNdc());
		if (ndc != null) {
			packaging.setNdc(ndc.getNdcCode());
		}

		pkgService.save(packaging);

		return new ModelAndView("redirect:/package");
	}

	@GetMapping("/{id}/copy")
	public ModelAndView copy(@PathVariable long id) {
		Packaging p = pkgService.findOne(id);
		Packaging copy = new Packaging();

		if (p != null) {
			copy.setDescription(p.getDescription());
			copy.setManufacturer(p.getManufacturer());
			copy.setNdc(p.getNdc());
			copy.setProduct(p.getProduct());
			copy.setUpc(p.getUpc());
			copy.setWholesaleAverageCost(p.getWholesaleAverageCost());
		}

		return packagingMav(copy);
	}

}
