package org.southwestohiogivecamp.druginventory.controllers;

import java.util.List;
import java.util.function.Function;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.services.InventoryService;
import org.southwestohiogivecamp.druginventory.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("location")
public class LocationController {

	@Autowired
	private LocationService locationService;

	@Autowired
	private InventoryService invService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	private ModelAndView mav(Location location) {
		ModelAndView model = new ModelAndView("location/editlocation");
		model.addObject("location", location);
		model.addObject("inventories", getInventories());

		return model;
	}

	@GetMapping(path = "/BarcodeReport/{inventoryType}")
	public void getLocationsBarcodeReport(@PathVariable InventoryType inventoryType, HttpServletResponse response) {
		try {
			locationService.createLocationBarcodeReport(inventoryType, response.getOutputStream());

			response.setHeader("content-type", "application/pdf");
			response.flushBuffer();
		} catch (Exception exception) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping()
	public ModelAndView getLocations() {
		List<Location> locations = locationService.findAll();
		ModelAndView model = new ModelAndView("location/locations");
		model.addObject("locations", locations);
		return model;
	}

	@GetMapping(path = "/{id}")
	public ModelAndView editLocation(@PathVariable long id) {
		Location location = locationService.findOne(id);
		return mav(location);
	}

	private List<Inventory> getInventories() {
		return invService.findAll();
	}

	@PostMapping(path = "/{id}")
	public ModelAndView editLocation(@PathVariable long id, @Valid Location location, BindingResult bindingResult) {
		return saveLocation("location/editlocation", id, location, bindingResult,
				savedLocation -> "redirect:/location");
	}

	@GetMapping(path = "/create")
	public ModelAndView createLocation() {
		return mav(new Location());
	}

	@PostMapping(path = "/create")
	public ModelAndView createLocation(Model model, @Valid Location location, BindingResult bindingResult) {
		return saveLocation("location/editlocation", 0, location, bindingResult, savedLocation -> "redirect:/location");
	}

	private ModelAndView saveLocation(String viewName, long locationId, Location location, BindingResult bindingResult,
			Function<Location, String> getViewNameCallback) {
		location.setLocationId(locationId);
		if (bindingResult.hasErrors()) {
			return mav(location);
		}
		locationService.save(location);

		String targetViewName = getViewNameCallback.apply(location);
		ModelAndView model = new ModelAndView(targetViewName);
		model.addObject("inventories", getInventories());
		return model;
	}
}
