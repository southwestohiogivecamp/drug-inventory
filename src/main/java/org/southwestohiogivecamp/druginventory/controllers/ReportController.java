package org.southwestohiogivecamp.druginventory.controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.services.InventoryService;
import org.southwestohiogivecamp.druginventory.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/report")
public class ReportController {

	@Autowired
	private ReportService reportService;
	@Autowired
	private InventoryService invService;
	
	private static final SimpleDateFormat df = ((SimpleDateFormat) DateFormat.getDateInstance());

	static {
		df.applyPattern("MM/dd/yyyy");
	}

	@RequestMapping
	public String index(final Model model) {
		model.addAttribute("inventories", invService.getInventoriesByType(InventoryType.INTERNAL));
		return "report/reports";
	}

	private Date parseDate(String date, Date defaultDate) {
		if (date != null) {
			try {
				return df.parse(date);
			} catch (java.text.ParseException pe) {
			}
		}

		return defaultDate;
	}

	@PostMapping("/excel")
	public void excelReport(int inventoryId, String startDate, String endDate,
			HttpServletResponse response) {
		Date start, end;

		end = parseDate(endDate, new Date());

		Calendar defaultStart = Calendar.getInstance();
		defaultStart.setTime(end);
		defaultStart.set(Calendar.MONTH, Calendar.JANUARY);
		defaultStart.set(Calendar.DATE, 0);

		start = parseDate(startDate, defaultStart.getTime());

		response.reset();

		try (OutputStream os = response.getOutputStream()) {
			String dateStr = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(new Date());

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "inline; filename=\"report" + dateStr + ".xls\"");

			reportService.writeTransactionReport(os, inventoryId, start, end);
			response.flushBuffer();
		} catch (IOException ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
}
