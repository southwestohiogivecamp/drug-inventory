package org.southwestohiogivecamp.druginventory.controllers;

import java.util.List;
import java.util.function.Function;

import javax.validation.Valid;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/inventory")
public class InventoryController {
	private static final String EDIT_INVENTORY_VIEW_NAME = "inventory/EditInventory";
	private static final String CREATE_INVENTORY_VIEW_NAME = EDIT_INVENTORY_VIEW_NAME;
	private static final String INVENTORY_PROPERTY_NAME = "inventory";

	@Autowired
	private InventoryService invService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@GetMapping()
	public ModelAndView getInventories() {
		List<Inventory> inventories = invService.findAll();

		ModelAndView model = new ModelAndView("inventory/Inventories");

		model.addObject("inventories", inventories);

		return model;
	}

	@GetMapping(path = "/{id}")
	public ModelAndView editInventory(@PathVariable int id) {
		Inventory inventory = invService.findOne(id);

		return createModelAndView(EDIT_INVENTORY_VIEW_NAME, inventory);
	}

	@PostMapping(path = "/{id}")
	public ModelAndView editInventory(@PathVariable int id, @Valid Inventory inventory, BindingResult bindingResult) {
		return saveInventory(EDIT_INVENTORY_VIEW_NAME, id, inventory, bindingResult,
				savedInventory -> "redirect:/inventory/");
	}

	@GetMapping(path = "/create")
	public ModelAndView createInventory() {
		return createModelAndView(CREATE_INVENTORY_VIEW_NAME, new Inventory());
	}

	@PostMapping(path = "/create")
	public ModelAndView createInventory(@Valid Inventory inventory, BindingResult bindingResult) {
		return saveInventory(CREATE_INVENTORY_VIEW_NAME, 0, inventory, bindingResult,
				savedInventory -> "redirect:/inventory/");
	}

	private ModelAndView saveInventory(String viewName, int id, Inventory inventory,
			BindingResult bindingResult, Function<Inventory, String> getViewNameCallback) {
		inventory.setInventoryId(id);
		if (bindingResult.hasErrors()) {
			return createModelAndView(viewName, inventory);
		}

		invService.save(inventory);

		String targetViewName = getViewNameCallback.apply(inventory);

		return new ModelAndView(targetViewName);
	}

	private ModelAndView createModelAndView(String template, Inventory inventory) {
		ModelAndView modelAndView = new ModelAndView(template);

		modelAndView.addObject(INVENTORY_PROPERTY_NAME, inventory);

		return modelAndView;
	}
}