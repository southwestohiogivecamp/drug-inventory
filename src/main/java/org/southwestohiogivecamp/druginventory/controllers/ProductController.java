package org.southwestohiogivecamp.druginventory.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.southwestohiogivecamp.druginventory.services.PackagingService;
import org.southwestohiogivecamp.druginventory.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService prodService;

	@Autowired
	private PackagingService pkgService;
	
	private static Logger log = LoggerFactory.getLogger(ProductController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	private ModelAndView productMav(final Product product, Long packagingId) {
		ModelAndView mav = new ModelAndView("product/productForm").addObject("product", product);
		if (packagingId != null) {
			Packaging pkg = pkgService.findOne(packagingId);
			if (pkg != null) {
				mav.addObject("packaging", pkg);
			}
		}
		
		return mav;
	}

	@GetMapping
	public ModelAndView index() {
		final ModelAndView mav = new ModelAndView("product/products");
		mav.addObject("products", prodService.findAll());
		return mav;
	}

	@GetMapping(path = "/create")
	public ModelAndView createNew(@RequestParam(required = false) Long packagingId) {
		return productMav(new Product(), packagingId);
	}

	@GetMapping(path = "/{id}")
	public ModelAndView index(@PathVariable long id) {
		Product prod = prodService.findOne(id);
		if (prod == null) {
			prod = new Product();
		}

		return productMav(prod, null);
	}

	@PostMapping(path = "/{id}")
	public ModelAndView update(@PathVariable long id, @RequestParam(required = false) Long packagingId, @Valid Product product,
			BindingResult bindingResult) {
		product.setProductId(id);
		log.debug("Got packagingId: {}", packagingId);
		if (bindingResult.hasErrors()) {
			log.debug("Oops, we have errors!");
			return productMav(product, packagingId);
		}

		Packaging pkg = null;
		if (packagingId != null) {
			pkg = pkgService.findOne(packagingId);
			if (pkg != null) {
				product.getPackages().add(pkg);
				pkg.setProduct(product);
			}
		}

		prodService.save(product);

		if(pkg == null) {
			return new ModelAndView("redirect:/product");
		}
		
		return new ModelAndView("redirect:/package");
	}

	@GetMapping(path = "/{id}/copy")
	public ModelAndView copy(@PathVariable long id) {
		Product prod = prodService.findOne(id);
		if (prod == null) {
			prod = new Product();
		}

		prod.setProductId(0);

		return productMav(prod, null);
	}

}
