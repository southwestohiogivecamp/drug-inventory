package org.southwestohiogivecamp.druginventory.controllers;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.southwestohiogivecamp.druginventory.domain.StockLocation;
import org.southwestohiogivecamp.druginventory.domain.Transaction;
import org.southwestohiogivecamp.druginventory.domain.TransactionStatus;
import org.southwestohiogivecamp.druginventory.model.PackageDetails;
import org.southwestohiogivecamp.druginventory.model.TransactionItemView;
import org.southwestohiogivecamp.druginventory.model.TransactionView;
import org.southwestohiogivecamp.druginventory.services.InventoryService;
import org.southwestohiogivecamp.druginventory.services.LocationService;
import org.southwestohiogivecamp.druginventory.services.PackagingService;
import org.southwestohiogivecamp.druginventory.services.StockService;
import org.southwestohiogivecamp.druginventory.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/transaction")
public class TransactionController {

	@Autowired
	private InventoryService invService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private PackagingService packagingService;
	@Autowired
	private StockService stockService;

	@Autowired
	private TransactionService txService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@GetMapping
	public String start() {
		return "transaction/start";
	}

	@GetMapping("/inbound")
	public ModelAndView inbound() {
		ModelAndView mav = new ModelAndView("transaction/transactionForm");

		mav.addObject("sources", invService.getInventoriesByType(InventoryType.INBOUND))
				.addObject("destinations", invService.getInventoriesByType(InventoryType.INTERNAL))
				.addObject("transaction", new Transaction()).addObject("txType", "Inbound");

		return mav;
	}

	@GetMapping("/outbound")
	public ModelAndView outbound() {
		ModelAndView mav = new ModelAndView("transaction/transactionForm");

		mav.addObject("sources", invService.getInventoriesByType(InventoryType.INTERNAL))
				.addObject("destinations", invService.getInventoriesByType(InventoryType.OUTBOUND))
				.addObject("transaction", new Transaction()).addObject("txType", "Outbound");

		return mav;
	}

	@GetMapping("/transfer")
	public ModelAndView transfer() {
		ModelAndView mav = new ModelAndView("transaction/transactionForm");

		mav.addObject("sources", invService.getInventoriesByType(InventoryType.INTERNAL))
				.addObject("destinations", invService.getInventoriesByType(InventoryType.INTERNAL))
				.addObject("transaction", new Transaction()).addObject("txType", "Transfer");

		return mav;
	}

	@GetMapping("/return")
	public ModelAndView doReturn() {
		ModelAndView mav = new ModelAndView("transaction/transactionForm");

		mav.addObject("sources", invService.getInventoriesByType(InventoryType.OUTBOUND))
				.addObject("destinations", invService.getInventoriesByType(InventoryType.INTERNAL))
				.addObject("transaction", new Transaction()).addObject("txType", "Return");

		return mav;
	}

	@PostMapping("/create")
	public String create(@RequestParam String description, @RequestParam Integer sourceInvId,
			@RequestParam Integer targetInvId) {
		Transaction trans = txService.create(description, sourceInvId, targetInvId);

		return String.format("redirect:/transaction/%d", trans.getTransactionId());
	}

	@GetMapping("/history")
	public ModelAndView index(@RequestParam(required = false) Date startDate,
			@RequestParam(required = false) Date endDate) {
		return new ModelAndView("transaction/transactions").addObject("transactions", txService.getAll(startDate, endDate));
	}
	

	@GetMapping(path = "/{id}")
	public ModelAndView index(@PathVariable long id, Model model) {
		return workspace(id, null, model);
	}

	private ModelAndView workspace(Long id, final PackageDetails pkgDetails, final Model model) {
		Transaction transaction = txService.get(id);
		if (transaction == null) {
			transaction = new Transaction();
		}

		return workspace(transaction, pkgDetails, model);
	}
	
	private ModelAndView workspace(Transaction transaction, final PackageDetails pkgDetails, final Model model) {
		final ModelAndView mav = new ModelAndView("transaction/transactionWorkspace");

		List<TransactionItemView> txViewItems = transaction.getTransactionItems().stream()
				.map(item -> {
					final StockLocation stockLoc = stockService.find(item.getStock(), item.getSource());
					final Integer available = stockLoc != null ? stockLoc.getCount() : null;
					
					return TransactionItemView.of(item, available);
				}).collect(Collectors.toList());

		final TransactionView txView = TransactionView.of(transaction)
				.withTransactionItemViews(txViewItems);

		PackageDetails packageDetails = pkgDetails;

		if (packageDetails == null) {
			packageDetails = new PackageDetails();
			if (model != null) {
				Map<String, Object> modelMap = model.asMap();
				packageDetails.setExpirationDate((Date) modelMap.get("expirationDate"));
				packageDetails.setSource((String) modelMap.get("source"));
				packageDetails.setLocation((String) modelMap.get("location"));
			}
		}

		if (isEmpty(packageDetails.getSource())) {
			List<Location> locations = transaction.getSource().getLocations();
			if (locations.size() == 1) {
				Location location = locations.get(0);
				packageDetails.setSource(location.getName());
			}
		}

		if (isEmpty(packageDetails.getLocation())) {
			List<Location> locations = transaction.getDestination().getLocations();
			if (locations.size() == 1) {
				Location location = locations.get(0);
				packageDetails.setLocation(location.getName());
			}
		}

		/*
		 * List<Location> destinations = transaction.getDestination().getLocations(); if
		 * (destinations.size() > 1) { Location empty = new Location();
		 * empty.setName("-- Please select a destination"); destinations.add(0, empty);
		 * }
		 */

		return mav.addObject("transaction", txView).addObject("packageDetails", packageDetails);
		// .addObject("destinations", destinations);
	}

	@PostMapping("/{id}/addItem")
	@Transactional
	public ModelAndView addItem(final @PathVariable Long id, final @Valid PackageDetails packagingDetails,
			final BindingResult bindingResult, final RedirectAttributes redirectAttrs) {
		final Transaction transaction = txService.get(id);
		if (transaction == null || transaction.getStatus() == TransactionStatus.COMPLETE) {
			// no good.
		}

		if (packagingDetails != null) {
			switch (transaction.getTransactionType()) {
			case INBOUND:
				return addInboundItem(transaction, packagingDetails, bindingResult, redirectAttrs);
			case OUTBOUND:
			case TRANSFER:
			case RETURN:
				return addOutboundItem(transaction, packagingDetails, bindingResult, redirectAttrs);
			}
		}

		return new ModelAndView(String.format("redirect:/transaction/%d", id));
	}
	
	private Location searchLocation(final Inventory context, final String location, 
			final String field, BindingResult bindingResult) {
		Location sourceLocation = null;

		List<Location> sourceLocations = locationService.searchLocation(context, location);
		if (sourceLocations.isEmpty()) {
			bindingResult.rejectValue(field, "location.not_found", "Unable to locate any location for given value.");
		} else if (sourceLocations.size() > 1) {
			bindingResult.rejectValue(field, "location.multiple_found",
					"Located multiple location matches. Please specify more information.");
		} else {
			sourceLocation = sourceLocations.get(0);
		}
		
		return sourceLocation;
	}
	
	private ModelAndView addOutboundItem(final Transaction tx, final PackageDetails packagingDetails,
			final BindingResult bindingResult, final RedirectAttributes redirectAttrs) {

		Location sourceLocation = null;
		if(isNotEmpty(packagingDetails.getSource())) {
			sourceLocation = searchLocation(tx.getSource(), packagingDetails.getSource(), "source", bindingResult);
		}

		Location selectedLocation = null;
		if (isNotEmpty(packagingDetails.getLocation())) {
			selectedLocation = searchLocation(tx.getDestination(), packagingDetails.getLocation(), "location", bindingResult);
		}
		
		if(bindingResult.hasErrors()) {
			return workspace(tx, packagingDetails, null);
		}

		List<Stock> stocks = stockService.searchStock(packagingDetails.getPackaging(), tx.getSource(), packagingDetails.getExpirationDate(), sourceLocation);
		if(stocks.isEmpty()) {
			bindingResult.reject("stock.search.not_found", "Unable to locate existing stock.");
		} else if(stocks.size() > 1) {
			bindingResult.reject("stock.search.multiple_found", "Unable to locate unique stock, please include more information.");
		} else {
			Stock stock = stocks.get(0);
			sourceLocation = sourceLocation != null ? sourceLocation : stock.getStockLocations().get(0).getLocation();
			txService.addOutboundItem(tx, stock, sourceLocation, selectedLocation, packagingDetails.getCount());

			if(sourceLocation != null) {
				redirectAttrs.addFlashAttribute("source", sourceLocation.getName());
			} else {
				redirectAttrs.addFlashAttribute("source", packagingDetails.getSource());
			}
			
			if(selectedLocation != null) {
				redirectAttrs.addFlashAttribute("location", selectedLocation.getName());
			} else {
				redirectAttrs.addFlashAttribute("location", packagingDetails.getLocation());
			}

			return new ModelAndView(String.format("redirect:/transaction/%d", tx.getTransactionId()));
		}

		return workspace(tx, packagingDetails, null);
	}

	private ModelAndView addInboundItem(final Transaction tx, final PackageDetails packagingDetails,
			final BindingResult bindingResult, final RedirectAttributes redirectAttrs) {

		List<Packaging> packages = packagingService.searchPackaging(packagingDetails.getPackaging(), true);
		if (packages.isEmpty()) {
			bindingResult.rejectValue("packaging", "packaging.not_found",
					"Unable to locate any packaging for given value.");
		} else if (packages.size() > 1) {
			bindingResult.rejectValue("packaging", "packaging.multiple_found",
					"Located multiple product matches. Please specify more information.");
		}

		Location sourceLocation = searchLocation(tx.getSource(), packagingDetails.getSource(), "source", bindingResult);
		Location selectedLocation = null;
		if (isNotEmpty(packagingDetails.getLocation())) {
			selectedLocation = searchLocation(tx.getDestination(), packagingDetails.getLocation(), "location", bindingResult);
		}

		if (!packagingDetails.isExpirationNull() && packagingDetails.getExpirationDate() == null) {
			bindingResult.rejectValue("expirationDate", "expirationDate.required", "Expiration Date is required.");
		}

		if (bindingResult.hasErrors()) {
			return workspace(tx, packagingDetails, null);
		}

		txService.addInboundItem(tx, packages.get(0), sourceLocation,
				packagingDetails.getExpirationDate(), selectedLocation, packagingDetails.getCount());

		redirectAttrs.addFlashAttribute("expirationDate", packagingDetails.getExpirationDate())
				.addFlashAttribute("source", packagingDetails.getSource());
		
		if(selectedLocation != null) {
			redirectAttrs.addFlashAttribute("location", selectedLocation.getName());
		}

		return new ModelAndView(String.format("redirect:/transaction/%d", tx.getTransactionId()));
	}
}
