package org.southwestohiogivecamp.druginventory.controllers;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.leftPad;

import java.util.List;

import javax.validation.Valid;

import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.StockLocation;
import org.southwestohiogivecamp.druginventory.domain.TransactionItem;
import org.southwestohiogivecamp.druginventory.domain.TransactionItemStatus;
import org.southwestohiogivecamp.druginventory.exception.StockLocationCountUnavailableException;
import org.southwestohiogivecamp.druginventory.model.TransactionItemView;
import org.southwestohiogivecamp.druginventory.model.VerificationModel;
import org.southwestohiogivecamp.druginventory.services.LocationService;
import org.southwestohiogivecamp.druginventory.services.StockService;
import org.southwestohiogivecamp.druginventory.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/transactionItem")
public class TransactionItemController {

	@Autowired
	private TransactionService txService;
	@Autowired
	private LocationService locService;
	@Autowired
	private StockService stockService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	private ModelAndView adjustMav(TransactionItem item, VerificationModel verify) {
		VerificationModel verificationModel = verify;
		if (verificationModel == null) {
			verificationModel = new VerificationModel();
			verificationModel.setCount(item.getCount());
			Location target = item.getTarget();
			verificationModel.setLocation(target != null ? target.getName() : null);
		}
		
		if(isEmpty(verificationModel.getLocation())) {
			List<Location> locations = item.getTransaction().getDestination().getLocations();
			if(locations.size() == 1) {
				verificationModel.setLocation(locations.get(0).getName());
			}
		}

		return new ModelAndView("transactionItem/adjust").addObject(verificationModel)
				.addObject("transactionDescription", item.getTransaction().getDescription())
				.addObject("transactionItem", TransactionItemView.of(item, 0));
	}

	@GetMapping(path = "/{id}")
	public ModelAndView adjustItem(@PathVariable long id) {
		final TransactionItem item = txService.getItem(id);

		return adjustMav(item, null);
	}

	@PostMapping(path = "/{id}")
	public ModelAndView adjustItemAction(@PathVariable long id, @Valid final VerificationModel verifyModel,
			final BindingResult bindingResult) {
		final TransactionItem item = txService.getItem(id);
		if(verifyModel.isCancel()) {
			return goBack(item);
		}

		Location location = null;
		/*
		 * if (isEmpty(verifyModel.getLocation())) {
		 * bindingResult.rejectValue("location", "location.not_null",
		 * "Please specify a location"); }
		 */
		if (isNotEmpty(verifyModel.getLocation())) {
			List<Location> locations = locService.searchLocation(item.getTransaction().getDestination(),
					verifyModel.getLocation());
			if (locations.isEmpty()) {
				bindingResult.rejectValue("location", "location.not_found",
						"Unable to locate any location for given value.");
			} else if (locations.size() > 1) {
				bindingResult.rejectValue("location", "location.multiple_found",
						"Located multiple location matches. Please be more specific.");
			} else {
				location = locations.get(0);
			}
		}

		if (bindingResult.hasErrors()) {
			return adjustMav(item, verifyModel);
		}

		if (verifyModel.isSplit()) {
			if (location == null) {
				bindingResult.rejectValue("location", "location.is_null", "Please specify a location");
			} else if(item.getTarget() != null && location.getLocationId() == item.getTarget().getLocationId()) {
				bindingResult.rejectValue("location", "location.not_changed", "A split without a location change will do nothong. Please specifiy a different location to split to.");
			}
			if (verifyModel.getCount() <= 0) {
				bindingResult.rejectValue("count", "count.minimum", "THe count must be greater than zero.");
			}
			if (bindingResult.hasErrors()) {
				return adjustMav(item, verifyModel);
			}

			txService.splitItem(item, location, verifyModel.getCount());
		} else {
			if (verifyModel.getCount() < 0) {
				bindingResult.rejectValue("count", "count.minimum", "THe count must be zero or greater.");
				return adjustMav(item, verifyModel);
			} else {
				txService.alterItem(item, location, verifyModel.getCount());
			}
		}

		return goBack(item);
	}

	@GetMapping(path = "/{id}/verify")
	public ModelAndView verifyItem(@PathVariable long id) {
		final TransactionItem item = txService.getItem(id);
		final StockLocation stockLoc = stockService.find(item.getStock(), item.getSource());
		final Integer available = stockLoc != null ? stockLoc.getCount() : null;

		return new ModelAndView("transactionItem/verify").addObject(new VerificationModel())
				.addObject("transactionDescription", item.getTransaction().getDescription())
				.addObject("transactionItem", TransactionItemView.of(item, available));
	}

	private ModelAndView verifyMav(final TransactionItem item, final VerificationModel verifyModel) {
		final StockLocation stockLoc = stockService.find(item.getStock(), item.getSource());
		final Integer available = stockLoc != null ? stockLoc.getCount() : null;
		return new ModelAndView("transactionItem/verify").addObject(verifyModel)
				.addObject("transactionDescription", item.getTransaction().getDescription())
				.addObject("transactionItem", TransactionItemView.of(item, available));
	}

	@PostMapping(path = "/{id}/verify")
	public ModelAndView verifyItemAction(@PathVariable long id, @Valid final VerificationModel verifyModel,
			final BindingResult bindingResult) {
		final TransactionItem item = txService.getItem(id);

		if (item.getStatus() == TransactionItemStatus.VERIFIED || verifyModel.isCancel()) {
			return goBack(item);
		}

		int numRequested = item.getCount();

		if (verifyModel.isForce()) {

			Location targetLocation = item.getTarget();
			String targetVerify = verifyModel.getLocation();
			if (isNotEmpty(targetVerify)) {
				List<Location> locations = locService.searchLocation(item.getTransaction().getDestination(),
						targetVerify);
				if (locations.isEmpty()) {
					bindingResult.rejectValue("location", "location.not_found",
							"Unable to locate any location for given value.");
				} else if (locations.size() > 1) {
					bindingResult.rejectValue("location", "location.multiple_found",
							"Located multiple location matches. Please be more specific.");
				} else {
					targetLocation = locations.get(0);
				}
			} else if (targetLocation == null) {
				bindingResult.rejectValue("location", "location.unspecified",
						"Location has not been specified for this transaction. Please provide a location.");
			}

			if (bindingResult.hasErrors()) {
				return verifyMav(item, verifyModel);
			}

			if (verifyModel.getCount() > 0) {
				numRequested = verifyModel.getCount();
				item.setCount(verifyModel.getCount());
			}

			item.setTarget(targetLocation);
		}

		if (verifyModel.isVerify()) {
			if (isEmpty(verifyModel.getPackaging())) {
				bindingResult.rejectValue("packaging", "", "Package must be specified for verification.");
			}
			if (isEmpty(verifyModel.getLocation())) {
				bindingResult.rejectValue("location", "", "Location must be specified for verification.");
			}

			if (bindingResult.hasErrors()) {
				return verifyMav(item, verifyModel);
			}

			Packaging pkg = item.getStock().getPackaging();
			String pkgVerify = verifyModel.getPackaging();
			if (pkgVerify.length() == 12 && !pkgVerify.equals(pkg.getUpc())) {
				bindingResult.rejectValue("packaging", "",
						"Packaging UPC code does not match item. Please verify your item.");
			} else if (pkgVerify.length() < 12 && !pkg.getNdc().equals(leftPad(pkgVerify, 11, '0'))) {
				bindingResult.rejectValue("packaging", "",
						"Packaging NDC code does not match item. Please verify your item.");
			} else if (pkgVerify.length() > 12) {
				bindingResult.rejectValue("packaging", "", "Only UPC or NDC can be used for verification.");
			}

			Location target = item.getTarget();
			Location targetLocation = target;
			String targetVerify = verifyModel.getLocation();
			if (target == null) {
				List<Location> locations = locService.searchLocation(item.getTransaction().getDestination(),
						targetVerify);
				if (locations.isEmpty()) {
					bindingResult.rejectValue("location", "location.not_found",
							"Unable to locate any location for given value.");
				} else if (locations.size() > 1) {
					bindingResult.rejectValue("location", "location.multiple_found",
							"Located multiple location matches. Please be more specific.");
				} else {
					targetLocation = locations.get(0);
				}
			} else {
				if (!Long.toString(target.getLocationId()).equals(targetVerify)
						&& !target.getName().contains(targetVerify)) {
					bindingResult.rejectValue("location", "", "Location does not match. Please verify your location.");
				}
			}

			int count = item.getCount();
			if (count != verifyModel.getCount()) {
				bindingResult.rejectValue("count", "", "Count does not match. Please verify your count.");
			}

			if (bindingResult.hasErrors()) {
				return verifyMav(item, verifyModel);
			}

			if (target == null && targetLocation != null) {
				item.setTarget(targetLocation);
			}

		}

		try {
			txService.completeItemVerification(item);
		} catch (StockLocationCountUnavailableException ex) {
			bindingResult.reject("", new Object[] { ex.getStockLocation().getCount(), numRequested },
					"There are not enough items available in inventory to satisfy this transaction. Only {0} of the requested {1} are available.");
			return verifyMav(item, verifyModel);
		}

		return goBack(item);
	}

	private ModelAndView goBack(final TransactionItem item) {
		return new ModelAndView("redirect:/transaction/" + item.getTransaction().getTransactionId());
	}

}
