package org.southwestohiogivecamp.druginventory.export;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.southwestohiogivecamp.druginventory.export.dto.ExportGroup;
import org.southwestohiogivecamp.druginventory.export.dto.ExportRow;
import org.southwestohiogivecamp.druginventory.export.dto.ExportRow.RowType;
import org.springframework.stereotype.Service;

@Service
public class ExportSpreadsheetService {
	
	public void exportTransactionReport(List<ExportGroup> groups, OutputStream out) throws IOException {
		try (Workbook wb = new HSSFWorkbook()) {
			Sheet sheet = wb.createSheet("new sheet");

			CreationHelper createHelper = wb.getCreationHelper();
			DataFormat format = wb.createDataFormat();
			BorderStyle borderStyle = BorderStyle.THIN;

			CellStyle styleDefault = wb.createCellStyle();
			styleDefault.setFillForegroundColor(IndexedColors.WHITE.index);
			styleDefault.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			styleDefault.setDataFormat(format.getFormat("General"));
			styleDefault.setBorderBottom(borderStyle);
			styleDefault.setBorderTop(borderStyle);
			styleDefault.setBorderLeft(borderStyle);
			styleDefault.setBorderRight(borderStyle);

			CellStyle styleNumber = wb.createCellStyle();
			styleNumber.cloneStyleFrom(styleDefault);
			styleNumber.setDataFormat(format.getFormat("#,##0;[Red](#,##0)"));

			CellStyle styleMoney = wb.createCellStyle();
			styleMoney.cloneStyleFrom(styleDefault);
			styleMoney.setDataFormat(format.getFormat("$#,##0.00;[Red]($#,##0.00)"));

			CellStyle styleDay = wb.createCellStyle();
			styleDay.cloneStyleFrom(styleDefault);
			styleDay.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yyyy"));

			CellStyle styleMonth = wb.createCellStyle();
			styleMonth.cloneStyleFrom(styleDefault);
			styleMonth.setDataFormat(createHelper.createDataFormat().getFormat("mmm-yy"));

			CellStyle styleBeginning = wb.createCellStyle();
			styleBeginning.cloneStyleFrom(styleDefault);
			styleBeginning.setDataFormat(format.getFormat("$#,##0.00;[Red]($#,##0.00)"));
			styleBeginning.setFillForegroundColor(IndexedColors.AQUA.index);

			CellStyle styleIns = wb.createCellStyle();
			styleIns.cloneStyleFrom(styleDefault);
			styleIns.setDataFormat(format.getFormat("$#,##0.00;[Red]($#,##0.00)"));
			styleIns.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.index);

			CellStyle styleOuts = wb.createCellStyle();
			styleOuts.cloneStyleFrom(styleDefault);
			styleOuts.setDataFormat(format.getFormat("$#,##0.00;[Red]($#,##0.00)"));
			styleOuts.setFillForegroundColor(IndexedColors.LAVENDER.index);

			CellStyle styleEndings = wb.createCellStyle();
			styleEndings.cloneStyleFrom(styleDefault);
			styleEndings.setDataFormat(format.getFormat("$#,##0.00;[Red]($#,##0.00)"));
			styleEndings.setFillForegroundColor(IndexedColors.WHITE.index);

			sheet.setDefaultColumnStyle(0, styleDefault);
			sheet.setDefaultColumnStyle(1, styleDefault);
			sheet.setDefaultColumnStyle(2, styleDefault);
			sheet.setDefaultColumnStyle(3, styleDefault);
			sheet.setDefaultColumnStyle(4, styleDay);
			sheet.setDefaultColumnStyle(5, styleDefault);
			sheet.setDefaultColumnStyle(6, styleDefault);
			sheet.setDefaultColumnStyle(7, styleNumber);
			sheet.setDefaultColumnStyle(8, styleDefault);
			sheet.setDefaultColumnStyle(9, styleMonth);
			sheet.setDefaultColumnStyle(10, styleDefault);
			sheet.setDefaultColumnStyle(11, styleDefault);
			sheet.setDefaultColumnStyle(12, styleDefault);
			sheet.setDefaultColumnStyle(13, styleDefault);
			sheet.setDefaultColumnStyle(14, styleDefault);
			sheet.setDefaultColumnStyle(15, styleMoney);
			sheet.setDefaultColumnStyle(16, styleMoney);
			sheet.setDefaultColumnStyle(17, styleBeginning);
			sheet.setDefaultColumnStyle(18, styleIns);
			sheet.setDefaultColumnStyle(19, styleOuts);
			sheet.setDefaultColumnStyle(20, styleEndings);

			int rownum = 0;

			// TODO freeze first 2 cols
			// TODO password protect file

			Row headerRow = sheet.createRow(rownum++);
			populateHeaderRow(wb, headerRow);

			CellStyle blank = wb.createCellStyle();
			blank.setFillForegroundColor(IndexedColors.WHITE.index);

			for (ExportGroup group : groups) {
				for (ExportRow transaction : group.getTransactions()) {
					Row row = sheet.createRow(rownum++);
					populateRow(wb, row, transaction, transaction.getRowType());
				}

				Row row = sheet.createRow(rownum++);
				populateRow(wb, row, group.getTotal(), RowType.TOTAL);

				Row blankRow = sheet.createRow(rownum++);
				blankRow.setRowStyle(blank);
			}

			wb.write(out);
		}
	}

	private void populateHeaderRow(Workbook wb, Row row) {
		int colNum = 0;

		CellStyle styleHeader = wb.createCellStyle();
		Font bold = wb.createFont();
		bold.setBold(true);
		styleHeader.setFont(bold);

		String[] headers = new String[] { "Location", "Medication Name", "Line", "Source", "DATE", "Brand Name", "Type of Medicine", "Quantity", "Unit Type", "Exp Date", "Conditions", "Dosing", "Verify",
				"NDC", "Manufacturer", "WAC", "EXT VALUE", "BEGINNING", "INS", "OUTS", "ENDING" };
		for (String header : headers) {
			Cell cell = row.createCell(colNum++);
			cell.setCellValue(header);
			cell.setCellStyle(styleHeader);
		}
	}

	private void populateRow(Workbook wb, Row row, ExportRow data, RowType type) {
		int colNum = 0;
		createCell(row, colNum++, data.getLocation(), type);
		createCell(row, colNum++, data.getMedicationName(), type);
		createCell(row, colNum++, data.getLine(), type);
		createCell(row, colNum++, data.getSource(), type);
		createCell(row, colNum++, data.getDate(), type);
		createCell(row, colNum++, data.getBrandName(), type);
		createCell(row, colNum++, data.getTypeOfMedicine(), type);
		createCell(row, colNum++, data.getQuantity(), type);
		createCell(row, colNum++, data.getUnitType(), type);
		createCell(row, colNum++, data.getExpDate(), type);
		createCell(row, colNum++, data.getConditions(), type);
		createCell(row, colNum++, data.getDosing(), type);
		createCell(row, colNum++, data.getVerify(), type);
		createCell(row, colNum++, data.getNdc(), type);
		createCell(row, colNum++, data.getManufacturer(), type);
		createCell(row, colNum++, data.getWac(), type);
		createCell(row, colNum++, data.getExtValue(), type);
		createCell(row, colNum++, data.getBeginning(), null);
		createCell(row, colNum++, data.getIns(), null);
		createCell(row, colNum++, data.getOuts(), null);
		createCell(row, colNum++, data.getEnding(), type);
	}

	private Cell createCell(Row row, int colNum, Object value, RowType type) {
		Cell cell = row.createCell(colNum);

		if (type != null) {
			CellStyle style = cell.getCellStyle();
			style.setFillForegroundColor(type.getColor());
			cell.setCellStyle(style);
		}

		if (value == null) {
			cell.setCellValue("");
		} else if (value instanceof Double) {
			cell.setCellValue((double) value);
		} else if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else if (value instanceof BigDecimal) {
			cell.setCellValue(((BigDecimal)value).doubleValue());
		} else {
			cell.setCellValue(value.toString());
		}

		return cell;
	}

}
