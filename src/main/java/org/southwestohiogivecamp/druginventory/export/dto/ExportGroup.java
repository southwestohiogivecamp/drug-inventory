package org.southwestohiogivecamp.druginventory.export.dto;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class ExportGroup {

	private LinkedList<ExportRow> transactions = new LinkedList<>();
	private ExportRow total;

	public List<ExportRow> getTransactions() {
		return Collections.unmodifiableList(transactions);
	}

	public void setTransactions(final List<ExportRow> transactions) {
		this.transactions = new LinkedList<>(transactions);
	}

	public boolean addTransaction(final ExportRow transaction) {
		final String line = transaction.getLine();
		if(StringUtils.isNoneEmpty(line) && line.equals("Tot")) {
			if(this.total != null) {
				final Integer newQuanity = this.total.getQuantity() + transaction.getQuantity();
				final BigDecimal newWac = this.total.getWac().add(transaction.getWac());
				
				this.total.setQuantity(newQuanity);
				this.total.setWac(newWac);
				this.total.setEnding(newWac);
			} else {
				this.total = transaction;
			}
			return true;
		}
		return transactions.add(transaction);
	}
	
	public void addStartingRow(ExportRow startingRow) {
		if(startingRow != null) {
			this.transactions.addFirst(startingRow);
		}
	}

	public ExportRow getTotal() {
		return total;
	}

	public void setTotal(ExportRow total) {
		this.total = total;
	}

}
