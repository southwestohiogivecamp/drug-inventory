package org.southwestohiogivecamp.druginventory.export.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.poi.ss.usermodel.IndexedColors;

public class ExportRow {

	public static enum RowType {
		INBOUND(IndexedColors.LIGHT_YELLOW.index), OUTBOUND(IndexedColors.LIGHT_CORNFLOWER_BLUE.index), TOTAL(
				IndexedColors.CORAL.index);

		private short color;

		private RowType(short color) {
			this.color = color;
		}

		public short getColor() {
			return color;
		}
	}

	private RowType rowType;
	private String location;
	private String medicationName;
	private String line;
	private String source;
	private Date date;
	private String brandName;
	private String typeOfMedicine;
	private Integer quantity;
	private String unitType;
	private Date expDate;
	private String conditions;
	private String dosing;
	private String verify;
	private String ndc;
	private String manufacturer;
	private BigDecimal wac;
	private BigDecimal extValue;
	private BigDecimal beginning;
	private BigDecimal ins;
	private BigDecimal outs;
	private BigDecimal ending;

	public RowType getRowType() {
		return rowType;
	}

	public void setRowType(RowType rowType) {
		this.rowType = rowType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMedicationName() {
		return medicationName;
	}

	public void setMedicationName(String medicationName) {
		this.medicationName = medicationName;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getTypeOfMedicine() {
		return typeOfMedicine;
	}

	public void setTypeOfMedicine(String typeOfMedicine) {
		this.typeOfMedicine = typeOfMedicine;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public String getDosing() {
		return dosing;
	}

	public void setDosing(String dosing) {
		this.dosing = dosing;
	}

	public String getVerify() {
		return verify;
	}

	public void setVerify(String verify) {
		this.verify = verify;
	}

	public String getNdc() {
		return ndc;
	}

	public void setNdc(String ndc) {
		this.ndc = ndc;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public BigDecimal getWac() {
		return wac;
	}

	public void setWac(BigDecimal wac) {
		this.wac = wac;
	}

	public BigDecimal getExtValue() {
		return extValue;
	}

	public void setExtValue(BigDecimal extValue) {
		this.extValue = extValue;
	}

	public BigDecimal getBeginning() {
		return beginning;
	}

	public void setBeginning(BigDecimal beginning) {
		this.beginning = beginning;
	}

	public BigDecimal getIns() {
		return ins;
	}

	public void setIns(BigDecimal ins) {
		this.ins = ins;
	}

	public BigDecimal getOuts() {
		return outs;
	}

	public void setOuts(BigDecimal outs) {
		this.outs = outs;
	}

	public BigDecimal getEnding() {
		return ending;
	}

	public void setEnding(BigDecimal ending) {
		this.ending = ending;
	}

	public ExportRow makeCopy() {
		final ExportRow newRow = new ExportRow();
		newRow.setRowType(this.rowType);
		newRow.setLocation(this.location);
		newRow.setMedicationName(this.medicationName);
		newRow.setLine(this.line);
		newRow.setSource(this.source);
		newRow.setDate(this.date);
		newRow.setBrandName(this.brandName);
		newRow.setTypeOfMedicine(this.typeOfMedicine);
		newRow.setQuantity(this.quantity);
		newRow.setUnitType(this.unitType);
		newRow.setExpDate(this.expDate);
		newRow.setConditions(this.conditions);
		newRow.setDosing(this.dosing);
		newRow.setVerify(this.verify);
		newRow.setNdc(this.ndc);
		newRow.setManufacturer(this.manufacturer);
		newRow.setWac(this.wac);
		newRow.setExtValue(this.extValue);
		newRow.setBeginning(this.beginning);
		newRow.setIns(this.ins);
		newRow.setOuts(this.outs);
		newRow.setEnding(this.ending);
		
		return newRow;
	}

}
