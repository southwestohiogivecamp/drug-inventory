package org.southwestohiogivecamp.druginventory.model;

import java.util.Date;

import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

public final class PackageDetails {

	private String packaging;
	private String source;
	private String location;

	@DateTimeFormat(pattern = "MM-yyyy")
	private Date expirationDate;
	private boolean expirationNull = false;

	@Min(1)
	private int count = 1;

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDte) {
		this.expirationDate = expirationDte;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public boolean isExpirationNull() {
		return expirationNull;
	}

	public void setExpirationNull(boolean expirationNull) {
		this.expirationNull = expirationNull;
	}

}
