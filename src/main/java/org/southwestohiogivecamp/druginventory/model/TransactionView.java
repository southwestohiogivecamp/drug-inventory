package org.southwestohiogivecamp.druginventory.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Transaction;
import org.southwestohiogivecamp.druginventory.domain.TransactionType;

public final class TransactionView {

	private Long transactionId;
	private String description;
	private TransactionType transactionType;

	private List<TransactionItemView> transactionItems = new ArrayList<TransactionItemView>();
	
	public static TransactionView of(Transaction transaction) {
		final TransactionView txView = new TransactionView();
		txView.setDescription(transaction.getDescription());
		txView.setTransactionType(transaction.getTransactionType());
		txView.setTransactionId(transaction.getTransactionId());
		
		return txView;
	}
	
	private TransactionView() {
		
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	private void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public List<TransactionItemView> getTransactionItems() {
		return Collections.unmodifiableList(transactionItems);
	}

	private void setTransactionItems(List<TransactionItemView> transactionItems) {
		this.transactionItems = transactionItems;
	}
	
	public TransactionView withTransactionItemViews(List<TransactionItemView> items) {
		this.setTransactionItems(new ArrayList<TransactionItemView>(items));
		return this;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	private void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

}
