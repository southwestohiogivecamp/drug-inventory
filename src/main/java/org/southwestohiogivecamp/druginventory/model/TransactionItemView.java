package org.southwestohiogivecamp.druginventory.model;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.southwestohiogivecamp.druginventory.domain.TransactionItem;
import org.southwestohiogivecamp.druginventory.domain.TransactionItemStatus;

public final class TransactionItemView {

	private Long transactionItemId;
	private String code;
	private String description;
	private String location;
	private String destination;
	private int availableCount;
	private int count;
	private TransactionItemStatus status;
	private Date expirationDate;
	
	private TransactionItemView() { }

	public static TransactionItemView of(final TransactionItem item, Integer availableCount) {
		TransactionItemView itemView = new TransactionItemView();
		Stock stock = item.getStock();
		Packaging packaging = stock.getPackaging();

		String code = packaging.getNdc();
		if (code == null) {
			code = packaging.getUpc();
		}

		itemView.setTransactionItemId(item.getTransactionItemId());
		itemView.setCode(code);
		itemView.setCount(item.getCount());
		itemView.setAvailableCount(availableCount != null ? availableCount : 0);
		itemView.setDescription(genDescription(packaging));
		itemView.setDestination(item.getTarget() != null ? item.getTarget().getName() : null);
		itemView.setExpirationDate(stock.getExpirationDate());
		itemView.setLocation(item.getSource() != null ? item.getSource().getName() : null);
		itemView.setStatus(item.getStatus());
		return itemView;
	}

	private static String genDescription(final Packaging packaging) {
		final List<String> parts = new ArrayList<>(3);
		final Product product = packaging.getProduct();
		if (product != null) {
			parts.add(product.getName());
			if (isNotEmpty(product.getActiveUnitStrength())) {
				parts.add(product.getActiveUnitStrength());
			}
		}
		parts.add(packaging.getDescription());

		return String.join(" ", parts);
	}

	public String getCode() {
		return code;
	}

	private void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	private void setLocation(String location) {
		this.location = location;
	}

	public String getDestination() {
		return destination;
	}

	private void setDestination(String destination) {
		this.destination = destination;
	}

	public int getAvailableCount() {
		return availableCount;
	}

	private void setAvailableCount(int availableCount) {
		this.availableCount = availableCount;
	}

	public int getCount() {
		return count;
	}

	private void setCount(int count) {
		this.count = count;
	}

	public TransactionItemStatus getStatus() {
		return status;
	}

	private void setStatus(TransactionItemStatus status) {
		this.status = status;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	private void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Long getTransactionItemId() {
		return transactionItemId;
	}

	private void setTransactionItemId(Long transactionItemId) {
		this.transactionItemId = transactionItemId;
	}

}
