package org.southwestohiogivecamp.druginventory.model;

public final class VerificationModel {

	private String location;
	private String packaging;
	private int count;
	private boolean split;

	private String action;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public boolean isSplit() {
		return split;
	}

	public void setSplit(boolean split) {
		this.split = split;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public boolean isCancel() {
		return "cancel".equals(this.action);
	}

	public boolean isForce() {
		return "force".equals(this.action);
	}

	public boolean isVerify() {
		return "verify".equals(this.action);
	}

}
