package org.southwestohiogivecamp.druginventory.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {
	
	private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @RequestMapping("/heartbeat")
    public String heartBeat() {
    	logger.debug("Lub-Dub");

        return "Lub-Dub";
    }

}