package org.southwestohiogivecamp.druginventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class DrugInventoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(DrugInventoryApplication.class, args);
    }

}
