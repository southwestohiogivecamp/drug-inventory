package org.southwestohiogivecamp.druginventory.repository;

import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackagingRepository extends JpaRepository<Packaging, Long> { }
