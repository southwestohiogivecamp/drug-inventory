package org.southwestohiogivecamp.druginventory.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.southwestohiogivecamp.druginventory.domain.TransactionItemStatus;
import org.southwestohiogivecamp.druginventory.domain.TransactionReportItem;
import org.springframework.stereotype.Repository;

@Repository
public class ReportRepository {
	@PersistenceContext
	private EntityManager manager;

	private static final String STOCK_TOTALS_PSQL = "select new org.southwestohiogivecamp.druginventory.domain.TransactionReportItem("
			+ "loc.name, loc.name, prod.name, inv.name, inv.name, prod.brandName, pkg.description, "
			+ "sloc.count, stock.expirationDate, prod.dosage, pkg.ndc, pkg.manufacturer, "
			+ "pkg.wholesaleAverageCost, inv.inventoryType, pkg.packagingId"
			+ ") from Stock as stock join stock.packaging as pkg join stock.stockLocations as sloc "
			+ "join sloc.location as loc join loc.inventory as inv join stock.packaging as pkg "
			+ "join pkg.product as prod where inv.inventoryId = :inventoryId "
			+ "order by loc.name, prod.name, pkg.description, stock.expirationDate";

	public List<TransactionReportItem> getCurrentInventoryTotals(int inventoryId) {
		return manager.createQuery(STOCK_TOTALS_PSQL, TransactionReportItem.class)
				.setParameter("inventoryId", inventoryId).getResultList();
	}

	private static final String REPORT_ITEM_PSQL = "select new org.southwestohiogivecamp.druginventory.domain.TransactionReportItem("
			+ "srcLoc.name, tgtLoc.name, prod.name, srcInv.name, tgtInv.name, tx.lastUpdated, prod.brandName, pkg.description, "
			+ "ti.count, stock.expirationDate, prod.dosage, pkg.ndc, pkg.manufacturer, "
			+ "pkg.wholesaleAverageCost, srcInv.inventoryType, pkg.packagingId"
			+ ") from TransactionItem as ti join ti.stock as stock join stock.packaging as pkg "
			+ "join pkg.product as prod join ti.transaction as tx "
			+ "join ti.source as srcLoc join srcLoc.inventory as srcInv "
			+ "join ti.target as tgtLoc join tgtLoc.inventory as tgtInv "
			+ "where ti.status = :verified and tx.lastUpdated > :begin and tx.lastUpdated <= :end "
			+ "and (srcInv.inventoryId = :inventoryId or tgtInv.inventoryId = :inventoryId) "
			+ "order by (case when tgtInv.inventoryType = 2 then tgtLoc.name else srcLoc.name end), "
			+ "prod.name, pkg.description, stock.expirationDate";

	public List<TransactionReportItem> getTransactionReportItems(int inventoryId, Date beginDate, Date endDate) {

		final TypedQuery<TransactionReportItem> query = manager.createQuery(REPORT_ITEM_PSQL, TransactionReportItem.class)
				.setParameter("begin", beginDate, TemporalType.DATE).setParameter("end", endDate, TemporalType.DATE)
				.setParameter("verified", TransactionItemStatus.VERIFIED).setParameter("inventoryId", inventoryId);

		return query.getResultList();

		/*
		 * String sql =
		 * "select srcLocation, dstLocation, medicationName, source, destination, date, brand, medicineType, quantity, expiration, dosing, ndc, manufacturer, wac, inventoryType, pack_id\n"
		 * + "from (\n" + "select\n" + " src.name srcLocation,\n" +
		 * " trgt.name dstLocation,\n" + " prod.name medicationName,\n" +
		 * " srcInv.name source,\n" + " trgtInv.name destination,\n" +
		 * " t.created date,\n" + " prod.brand_Name brand,\n" +
		 * " pack.description medicineType,\n" + " ti.count quantity,\n" +
		 * " stock.expiration_Date expiration,\n" + " prod.dosage dosing,\n" +
		 * " pack.ndc ndc,\n" + " pack.manufacturer manufacturer,\n" +
		 * " pack.wholesale_Average_Cost wac,\n" +
		 * " srcInv.inventory_Type inventoryType,\n" +
		 * " prod.product_id as product_id,\n" +
		 * " stock.created_Date as stock_created,\n" + " pack.packaging_id as pack_id\n"
		 * + " from Product prod\n" + " join Packaging pack using(product_id)" +
		 * " join Stock stock using(packaging_id)" +
		 * " join Transaction_Item ti using(stock_id)" +
		 * " join Transaction t using(transaction_id)" +
		 * " join Location src on ti.source_location_id = src.location_id\n" +
		 * " join Location trgt on ti.target_location_id = trgt.location_id\n" +
		 * " join Inventory srcInv on src.inventory_id = srcInv.inventory_id\n" +
		 * " join Inventory trgtInv on trgt.inventory_id = trgtInv.inventory_id\n" +
		 * " where ti.status = :verified\n" + " and t.created < :endDate\n" +
		 * " and t.created >= :createDate\n" + " UNION ALL\n" + " select\n" +
		 * " loc.name srcLocation,\n" + " loc.name dstLocation,\n" +
		 * " prod.name medicationName,\n" + " inv.name source,\n" +
		 * " inv.name destination,\n" + " null date t.created,\n" +
		 * " prod.brand_Name brand,\n" + " pack.description medicineType,\n" +
		 * " sl.count quantity,\n" + " stock.expiration_Date expiration,\n" +
		 * " prod.dosage dosing,\n" + " pack.ndc ndc,\n" +
		 * " pack.manufacturer manufacturer,\n" + " pack.wholesale_Average_Cost wac,\n"
		 * + " inv.inventory_Type inventoryType,\n" +
		 * " prod.product_id as product_id,\n" +
		 * " stock.created_Date as stock_created,\n" + " pack.packaging_id as pack_id\n"
		 * + " from Product prod\n" + " join Packaging pack using(product_id)" +
		 * " join Stock stock using(packaging_id)" +
		 * " join Stock_Location sl using(stock_id)" +
		 * " join Location loc using(location_id)" +
		 * " join Inventory inv using(inventory_id)" + " where \n" + " not exists(\n" +
		 * " select null\n" + " from Transaction t2\n" +
		 * " join Transaction_Item ti2 using(transaction_id)" +
		 * " where ti2.status = :verified\n" + " and t2.created < :endDate\n" +
		 * " and t2.created >= :createDate\n" +
		 * " and ti2.source_location_id = sl.location_id and ti2.stock_id = sl.stock_id\n"
		 * + " )\n" + " and exists(\n" + " select null\n" + " from Transaction t2\n" +
		 * " join Transaction_Item ti2 using(transaction_id)" +
		 * " where ti2.status = :verified\n" +
		 * " and ti2.source_location_id = sl.location_id and ti2.stock_id = sl.stock_id\n"
		 * + " )\n" + " \n" + " ) z\n" + "  order by z.product_id,\n" + " z.pack_id,\n"
		 * + " z.date,\n" + " z.stock_created"; Query query =
		 * manager.createNativeQuery(sql, "TransactionReportItem");
		 * query.setParameter("endDate", endDate, TemporalType.DATE);
		 * query.setParameter("createDate", beginDate, TemporalType.DATE);
		 * query.setParameter("verified", TransactionItemStatus.VERIFIED.name()); return
		 * query.getResultList();
		 */
	}

}
