package org.southwestohiogivecamp.druginventory.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.southwestohiogivecamp.druginventory.domain.Transaction;
import org.southwestohiogivecamp.druginventory.domain.Transaction_;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionRepository {

	@PersistenceContext
	private EntityManager em;
	
	public List<Transaction> TransactionsByDate(final Date startDate, final Date endDate) {
		final CriteriaBuilder bldr = em.getCriteriaBuilder();

		final CriteriaQuery<Transaction> query = bldr.createQuery(Transaction.class);
		final Root<Transaction> tx = query.from(Transaction.class);
		query.select(tx);
		
		final List<Predicate> predicates = new ArrayList<>();
		
		if(startDate != null) {
			predicates.add(bldr.greaterThanOrEqualTo(tx.get(Transaction_.created), startDate));
		}
		
		if(endDate != null) {
			predicates.add(bldr.lessThanOrEqualTo(tx.get(Transaction_.created), endDate));
		}
		
		return em.createQuery(query.where(predicates.toArray(new Predicate[0]))).getResultList();
		
		/*
		return em.createQuery("from Transaction tx where tx.created between :startDate and :endDate", Transaction.class)
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				.getResultList();
				*/
	}

	public void save(Transaction item) {
		em.persist(item);
	}

	public Transaction findOne(long id) {
		return em.find(Transaction.class, id);
	}

	public void delete(long id) {
		Transaction it = findOne(id);

		if (it != null) {
			em.remove(it);
		}
	}

	public Transaction find(final String name) {
	    return em.createQuery("from Transaction tx where tx.name=:name", Transaction.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public List<Transaction> getActive() {
        return em.createQuery("from Transaction tx where tx.active=true", Transaction.class)
                .getResultList();
    }
}