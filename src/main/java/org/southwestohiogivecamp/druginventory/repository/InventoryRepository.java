package org.southwestohiogivecamp.druginventory.repository;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<Inventory, Integer> { }