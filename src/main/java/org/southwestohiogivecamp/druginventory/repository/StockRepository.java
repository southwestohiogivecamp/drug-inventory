package org.southwestohiogivecamp.druginventory.repository;

import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> { }