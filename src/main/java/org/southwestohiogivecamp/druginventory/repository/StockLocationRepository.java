package org.southwestohiogivecamp.druginventory.repository;

import org.southwestohiogivecamp.druginventory.domain.StockLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockLocationRepository extends JpaRepository<StockLocation, Long> {
}