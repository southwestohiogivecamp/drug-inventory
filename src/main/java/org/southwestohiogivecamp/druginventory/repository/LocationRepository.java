package org.southwestohiogivecamp.druginventory.repository;

import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class LocationRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public Location findOne(long id) {
        return entityManager.find(Location.class, id);
    }

    public List<Location> findAll() {
        return entityManager.createQuery("FROM Location", Location.class)
                .getResultList();
    }

    public List<Location> findByType(InventoryType inventoryType) {
        return entityManager.createQuery("FROM Location l WHERE l.inventory.inventoryType = :inventoryType ORDER BY l.name", Location.class)
                .setParameter("inventoryType", inventoryType)
                .getResultList();
    }

    public void save(Location location) {
		if(location.getLocationId() > 0) {
			entityManager.merge(location);
		} else {
			entityManager.persist(location);
		}
    }

    public void delete(long id) {
        entityManager.remove(findOne(id));
    }
}
