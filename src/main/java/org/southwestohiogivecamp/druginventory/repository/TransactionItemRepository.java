package org.southwestohiogivecamp.druginventory.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.southwestohiogivecamp.druginventory.domain.TransactionItem;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionItemRepository {

	@PersistenceContext
	private EntityManager em;

	public void save(TransactionItem item) {
		em.persist(item);
	}

	public TransactionItem findOne(long id) {
		return em.find(TransactionItem.class, id);
	}

	public void delete(long id) {
		TransactionItem it = findOne(id);

		if (it != null) {
			em.remove(it);
		}
	}

}