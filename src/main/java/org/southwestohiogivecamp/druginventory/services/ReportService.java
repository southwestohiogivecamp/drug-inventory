package org.southwestohiogivecamp.druginventory.services;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.TransactionReportItem;
import org.southwestohiogivecamp.druginventory.export.ExportSpreadsheetService;
import org.southwestohiogivecamp.druginventory.export.dto.ExportGroup;
import org.southwestohiogivecamp.druginventory.export.dto.ExportRow;
import org.southwestohiogivecamp.druginventory.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ReportService {

	@Autowired
	private ReportRepository repo;
	@Autowired
	private ExportSpreadsheetService exportSpreadsheetService;

	public void writeTransactionReport(OutputStream out, int inventoryId, Date beginDate, Date endDate)
			throws IOException {
		List<ExportGroup> groups = getTransactionReport(inventoryId, beginDate, endDate);

		exportSpreadsheetService.exportTransactionReport(groups, out);
	}

	public List<ExportGroup> getTransactionReport(int inventoryId, Date beginDate, Date endDate) {
		List<ExportGroup> groups = new LinkedList<>();

		ExportGroup currentGroup = null;
		long currentPackId = -1;
		String currentLoc = "";

		final List<TransactionReportItem> items = repo.getTransactionReportItems(inventoryId, beginDate, endDate);
		final List<TransactionReportItem> totals = repo.getCurrentInventoryTotals(inventoryId);

		final ReportItemProvider reportItemProvider = new ReportItemProvider(items, totals);
		TransactionReportItem item = reportItemProvider.get();
		ExportRow row = null;
		while (item != null) {
			if (!currentLoc.equals(item.getLocationForType()) || item.getPackId() != currentPackId) {
				if (currentGroup != null) {
					balanceGroup(currentGroup);
				}

				// create the new one
				currentGroup = new ExportGroup();
				groups.add(currentGroup);
			}

			row = createExportRow(item);
			currentGroup.addTransaction(row);

			currentPackId = item.getPackId();
			currentLoc = item.getLocationForType();

			item = reportItemProvider.get();
		}

		if (currentGroup != null) {
			balanceGroup(currentGroup);
		}

		return groups;
	}

	private void balanceGroup(ExportGroup currentGroup) {
		BigDecimal total = BigDecimal.ZERO;
		int quantity = 0;
		for (ExportRow row : currentGroup.getTransactions()) {
			quantity += row.getQuantity();
			if (row.getExtValue() != null) {
				total = total.add(row.getExtValue());
			}
		}

		final ExportRow firstRow = (!currentGroup.getTransactions().isEmpty()) ? currentGroup.getTransactions().get(0)
				: null;
		ExportRow totalRow = currentGroup.getTotal();

		if (firstRow != null && totalRow == null) {
			totalRow = createTotalRow(total, quantity, firstRow);
			currentGroup.setTotal(totalRow);
		}

		int startingQuantity = totalRow.getQuantity() - quantity;
		if (startingQuantity > 0) {
			final BigDecimal startingTotal = totalRow.getWac().subtract(total);
			final ExportRow startingRow = createStartingRow(startingTotal, startingQuantity, totalRow);
			currentGroup.addStartingRow(startingRow);
		}
	}

	private ExportRow createExportRow(TransactionReportItem item) {
		ExportRow row = new ExportRow();

		ExportRow.RowType rowType;
		BigDecimal ins = null;
		BigDecimal outs = null;
		BigDecimal end = null;
		BigDecimal extValue = item.getExtValue();
		String line = null;
		int quantity = item.getQuantity();
		Date date = item.getDate();
		String source = item.getSrcLocation();
		final String location = item.getLocationForType();
		if (date == null) {
			rowType = ExportRow.RowType.TOTAL;
			end = extValue;
			line = "Tot";
			date = new Date();
		} else if (item.getInventoryType() == InventoryType.OUTBOUND) {
			rowType = ExportRow.RowType.OUTBOUND;
			extValue = extValue.negate();
			outs = extValue;
			quantity = quantity * -1;
			source = item.getDstLocation();
		} else {
			rowType = ExportRow.RowType.INBOUND;
			ins = extValue;
		}

		row.setRowType(rowType);
		row.setLocation(location);
		row.setMedicationName(item.getMedicationName());
		row.setLine(line);
		row.setSource(source);
		row.setDate(date);
		row.setBrandName(item.getBrand());
		row.setTypeOfMedicine(item.getMedicineType());
		row.setQuantity(quantity);
		row.setUnitType(null);
		row.setExpDate(item.getExpiration());
		row.setConditions(null);
		row.setDosing(item.getDosing());
		row.setVerify(null);
		row.setNdc(item.getNdc());
		row.setManufacturer(item.getManufacturer());
		row.setWac(item.getWac());
		row.setExtValue(extValue);
		row.setBeginning(null);
		row.setIns(ins);
		row.setOuts(outs);
		row.setEnding(end);

		return row;
	}

	private ExportRow createTotalRow(BigDecimal total, int quantity, ExportRow firstRow) {
		return createDummyRow("Tot", total, quantity, firstRow);
	}

	private ExportRow createStartingRow(BigDecimal total, int quantity, ExportRow firstRow) {
		return createDummyRow(null, total, quantity, firstRow);
	}

	private ExportRow createDummyRow(String line, BigDecimal total, int quantity, ExportRow firstRow) {
		final ExportRow row = firstRow.makeCopy();

		row.setRowType(ExportRow.RowType.TOTAL);
		row.setLine(line);
		row.setQuantity(quantity);
		row.setExtValue(total);
		row.setEnding(total);
		row.setDate(new Date());
		row.setExpDate(null);

		return row;
	}

	private static class ReportItemComparator implements Comparator<TransactionReportItem> {
		@Override
		public int compare(final TransactionReportItem total, final TransactionReportItem item) {
			int comparison = 0;
			comparison = total.getSrcLocation().compareTo(item.getLocationForType());
			if (comparison == 0) {
				comparison = total.getMedicationName().compareTo(item.getMedicationName());
			}
			if (comparison == 0) {
				comparison = total.getMedicineType().compareTo(item.getMedicineType());
			}
			return comparison;
		}
	}

	private static class ReportItemProvider implements Supplier<TransactionReportItem> {
		private final List<TransactionReportItem> itemList;
		private final List<TransactionReportItem> totalList;

		private final Iterator<TransactionReportItem> itemIterator;
		private final Iterator<TransactionReportItem> totalIterator;

		private TransactionReportItem currentItem = null;
		private TransactionReportItem currentTotal = null;
		private TransactionReportItem lastTotal = null;

		private static final ReportItemComparator comparator = new ReportItemComparator();

		public ReportItemProvider(final List<TransactionReportItem> itemList,
				final List<TransactionReportItem> totalList) {
			this.itemList = Collections.unmodifiableList(itemList);
			this.totalList = Collections.unmodifiableList(totalList);
			this.itemIterator = this.itemList.iterator();
			this.totalIterator = this.totalList.iterator();
			nextItem();
			nextTotal();
		}

		private TransactionReportItem doNext(Iterator<TransactionReportItem> iterator) {
			if (iterator.hasNext()) {
				return iterator.next();
			}
			return null;
		}

		private TransactionReportItem nextItem() {
			final TransactionReportItem lastItem = this.currentItem;
			this.currentItem = doNext(this.itemIterator);
			return lastItem;
		}

		private TransactionReportItem nextTotal() {
			this.lastTotal = this.currentTotal;
			this.currentTotal = doNext(this.totalIterator);
			return this.lastTotal;
		}

		@Override
		public TransactionReportItem get() {
			if (this.currentItem == null) {
				return nextTotal();
			}

			if (this.currentTotal == null
					|| (this.lastTotal != null && comparator.compare(this.lastTotal, this.currentItem) == 0)
					|| comparator.compare(this.currentTotal, this.currentItem) > 0) {
				return nextItem();
			}

			return nextTotal();
		}
	}
}
