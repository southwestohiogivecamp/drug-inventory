package org.southwestohiogivecamp.druginventory.services;

import java.io.OutputStream;
import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;

public interface LocationService {

	void createLocationBarcodeReport(InventoryType inventoryType, OutputStream outputStream);

	Location findOne(Long id);

	List<Location> findAll();

	void save(Location location);
	
	List<Location> searchLocation(Inventory context, String locationSearch);
	
}