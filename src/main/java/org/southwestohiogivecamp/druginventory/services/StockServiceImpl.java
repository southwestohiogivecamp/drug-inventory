package org.southwestohiogivecamp.druginventory.services;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNumeric;
import static org.apache.commons.lang3.StringUtils.leftPad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Location_;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Packaging_;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.southwestohiogivecamp.druginventory.domain.Product_;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.southwestohiogivecamp.druginventory.domain.StockLocation;
import org.southwestohiogivecamp.druginventory.domain.StockLocation_;
import org.southwestohiogivecamp.druginventory.domain.Stock_;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class StockServiceImpl implements StockService {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Stock findOrCreate(Packaging packaging, Date expirationDate) {
		List<Stock> stockItems = null;

		if (expirationDate != null) {
			stockItems = em.createNamedQuery("findByPackagingAndExpiration", Stock.class)
					.setParameter("packaging", packaging).setParameter("expDate", expirationDate).getResultList();
		} else {
			stockItems = em.createNamedQuery("findByPackagingWithNullExpiration", Stock.class)
					.setParameter("packaging", packaging).getResultList();

		}

		Stock stock = null;
		if (stockItems.isEmpty()) {
			stock = new Stock();
			stock.setCreatedDate(new Date());
			stock.setExpirationDate(expirationDate);
			stock.setPackaging(packaging);

			em.persist(stock);
		} else {
			stock = stockItems.get(0);
		}

		return stock;
	}

	@Override
	public List<Stock> searchStock(String packaging, Inventory context, Date expDate, Location source) {
		if (isEmpty(packaging)) {
			return Collections.emptyList();
		}

		final CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Stock> crit = cb.createQuery(Stock.class);
		final Root<Stock> stock = crit.from(Stock.class);
		final Join<Stock, StockLocation> stocklocJoin = stock.join(Stock_.stockLocations, JoinType.INNER);
		final Join<StockLocation, Location> locJoin = stocklocJoin.join(StockLocation_.location);
		
		List<Predicate> ands = new ArrayList<>();
		
		ands.add(cb.equal(locJoin.get(Location_.inventory), context));

		final Join<Stock, Packaging> pkgJoin = stock.join(Stock_.packaging);

		if (isNumeric(packaging)) {
			if (packaging.length() == 12) {
				ands.add(cb.equal(pkgJoin.get(Packaging_.upc), packaging));
			} else if (packaging.length() < 12) {
				String paddedPkg = leftPad(packaging, 11, '0');
				ands.add(cb.equal(pkgJoin.get(Packaging_.ndc), paddedPkg));
			}
		} else {
			final Join<Packaging, Product> prodJoin = pkgJoin.join(Packaging_.product);
			final String likeString = String.format("%%%s%%", packaging);
			ands.add(cb.or(cb.like(pkgJoin.get(Packaging_.description), likeString),
					cb.like(prodJoin.get(Product_.name), likeString)));
		}

		if (expDate != null) {
			ands.add(cb.equal(stock.get(Stock_.expirationDate), expDate));
		}

		if (source != null) {
			ands.add(cb.equal(stocklocJoin.get(StockLocation_.location), source));
			ands.add(cb.greaterThan(stocklocJoin.get(StockLocation_.count), 0));
		}

		return em.createQuery(crit.where(ands.toArray(new Predicate[0]))).getResultList();
	}

	@Override
	public StockLocation find(Stock stock, Location location) {
		List<StockLocation> locations =  em.createQuery("from StockLocation stockLoc where stockLoc.stock = :stock and stockLoc.location = :location", StockLocation.class)
				.setParameter("stock", stock)
				.setParameter("location", location)
				.getResultList();
		
		if(locations.isEmpty()) {
			return null;
		}
		
		return locations.get(0);
	}

	@Override
	public StockLocation create(Stock stock, Location location, int count) {
		StockLocation newLoc = new StockLocation();
		newLoc.setStock(stock);
		newLoc.setLocation(location);
		newLoc.setCount(count);
		
		em.persist(newLoc);
		
		return newLoc;
	}
}
