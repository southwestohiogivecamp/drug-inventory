package org.southwestohiogivecamp.druginventory.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private InventoryRepository repo;
	
	/* (non-Javadoc)
	 * @see org.southwestohiogivecamp.druginventory.services.InventoryService#getInventoriesByType(org.southwestohiogivecamp.druginventory.domain.InventoryType)
	 */
	@Override
	public List<Inventory> getInventoriesByType(InventoryType type) {
		return em.createQuery("from Inventory i where i.inventoryType = :type", Inventory.class)
				.setParameter("type", type)
				.getResultList();
	}

	/* (non-Javadoc)
	 * @see org.southwestohiogivecamp.druginventory.services.InventoryService#findOne(java.lang.Long)
	 */
	@Override
	public Inventory findOne(int id) {
		return repo.findOne(id);
	}

	/* (non-Javadoc)
	 * @see org.southwestohiogivecamp.druginventory.services.InventoryService#findAll()
	 */
	@Override
	public List<Inventory> findAll() {
		return repo.findAll();
	}

	/* (non-Javadoc)
	 * @see org.southwestohiogivecamp.druginventory.services.InventoryService#save(org.southwestohiogivecamp.druginventory.domain.Inventory)
	 */
	@Override
	public void save(Inventory inventory) {
		repo.save(inventory);
	}

}
