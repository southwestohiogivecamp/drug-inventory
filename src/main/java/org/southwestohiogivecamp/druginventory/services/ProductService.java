package org.southwestohiogivecamp.druginventory.services;

import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Product;

public interface ProductService {

	Product findOne(Long id);

	List<Product> findAll();

	void save(Product product);

}
