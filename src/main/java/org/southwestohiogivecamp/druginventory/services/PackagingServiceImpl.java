package org.southwestohiogivecamp.druginventory.services;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNumeric;
import static org.apache.commons.lang3.StringUtils.leftPad;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.southwestohiogivecamp.druginventory.domain.NdcCode;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Packaging_;
import org.southwestohiogivecamp.druginventory.repository.PackagingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PackagingServiceImpl implements PackagingService {

	@Autowired
	private PackagingRepository repo;

	@PersistenceContext
	private EntityManager em;

	@Override
	public Packaging findOne(Long id) {
		return repo.findOne(id);
	}

	@Override
	public List<Packaging> findAll() {
		return em.createQuery("select pkg from Packaging pkg left join fetch pkg.product", Packaging.class).getResultList();
	}

	@Override
	public void save(Packaging product) {
		repo.save(product);
	}

	@Override
	public List<Packaging> searchPackaging(String packageSearch, boolean createIfNotFound) {
		if (isEmpty(packageSearch)) {
			return Collections.emptyList();
		}

		final CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Packaging> crit = cb.createQuery(Packaging.class);
		final Root<Packaging> pkg = crit.from(Packaging.class);

		boolean digitSearch = false;
		if (isNumeric(packageSearch)) {
			digitSearch = true;
			// If the numer has 12 digits, it's a UPC.
			if (packageSearch.length() == 12) {
				crit = crit.where(cb.equal(pkg.get(Packaging_.upc), packageSearch));
			} else if (packageSearch.length() < 12) {
				String paddedSearch = leftPad(packageSearch, 11, '0');
				crit = crit.where(cb.equal(pkg.get(Packaging_.ndc), paddedSearch));
			}
		} else {
			crit = crit.where(cb.like(pkg.get(Packaging_.description), String.format("%%%s%%", packageSearch)));
		}

		List<Packaging> results = em.createQuery(crit).getResultList();
		if (createIfNotFound && digitSearch && results.isEmpty() && packageSearch.length() <= 12) {
			String packageName = MessageFormat.format("Pending packaging - {0,date,yyyy-MM-dd HH:mm:ss}", new Date());

			Packaging newPackaging = new Packaging();
			newPackaging.setDescription(packageName);
			if (packageSearch.length() == 12) {
				newPackaging.setUpc(packageSearch);
			} else if (packageSearch.length() < 12) {
				NdcCode ndc = NdcCode.codeFor(packageSearch);
				if(ndc != null) {
					newPackaging.setNdc(ndc.getNdcCode());
				}
			}

			save(newPackaging);

			results = new ArrayList<>(1);
			results.add(newPackaging);
		}

		return results;
	}
}
