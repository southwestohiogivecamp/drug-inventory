package org.southwestohiogivecamp.druginventory.services;

import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Packaging;

public interface PackagingService {

	Packaging findOne(Long id);

	List<Packaging> findAll();

	void save(Packaging product);
	
	List<Packaging> searchPackaging(String packageSearch, boolean createIfNotFound);

}
