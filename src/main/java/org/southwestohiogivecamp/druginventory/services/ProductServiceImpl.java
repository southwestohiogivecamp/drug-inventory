package org.southwestohiogivecamp.druginventory.services;

import java.util.List;

import javax.transaction.Transactional;

import org.southwestohiogivecamp.druginventory.domain.Product;
import org.southwestohiogivecamp.druginventory.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository repo;

	@Override
	public Product findOne(Long id) {
		return repo.findOne(id);
	}

	@Override
	public List<Product> findAll() {
		return repo.findAll();
	}

	@Override
	public void save(Product product) {
		repo.save(product);
	}
}
