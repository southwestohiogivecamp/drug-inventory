package org.southwestohiogivecamp.druginventory.services;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.southwestohiogivecamp.druginventory.domain.StockLocation;
import org.southwestohiogivecamp.druginventory.domain.Transaction;
import org.southwestohiogivecamp.druginventory.domain.TransactionItem;
import org.southwestohiogivecamp.druginventory.domain.TransactionItemStatus;
import org.southwestohiogivecamp.druginventory.exception.StockLocationCountUnavailableException;
import org.southwestohiogivecamp.druginventory.repository.InventoryRepository;
import org.southwestohiogivecamp.druginventory.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private StockService stockService;

	@PersistenceContext
	private EntityManager em;

	@Override
	public Transaction get(long id) {
		return em.createQuery("from Transaction t left join fetch t.transactionItems where t.transactionId = :id",
				Transaction.class).setParameter("id", id).getSingleResult();
	}

	@Override
	public Transaction get(final String name) {
		return transactionRepository.find(name);
	}

	@Override
	public List<Transaction> getAllActive() {
		return transactionRepository.getActive();
	}

	@Override
	public List<Transaction> getAll(final Date start, final Date end) {
		return transactionRepository.TransactionsByDate(start, end);
	}

	@Override
	public Transaction create(final String description, final int sourceInvId, final int targetInvId) {
		Inventory sourceEntity = inventoryRepository.findOne(sourceInvId);
		Inventory targetEntity = inventoryRepository.findOne(targetInvId);
		Date now = new Date();

		Transaction transaction = new Transaction();

		transaction.setCreated(now);
		transaction.setDescription(description);
		transaction.setLastUpdated(now);
		transaction.setSource(sourceEntity);
		transaction.setDestination(targetEntity);

		transactionRepository.save(transaction);

		return transaction;
	}

	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public TransactionItem addInboundItem(Transaction transaction, Packaging packaging, Location source,
			Date expirationDate, Location location, int count) {
		if (transaction == null) {
			throw new IllegalArgumentException("Transaction cannot be null");
		}
		if (packaging == null) {
			throw new IllegalArgumentException("Packaging cannot be null");
		}
		Stock stockItem = stockService.findOrCreate(packaging, expirationDate);

		return addStockToTransaction(transaction, stockItem, source, location, count, false);
	}

	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public TransactionItem addOutboundItem(Transaction transaction, Stock stock, Location source, Location location,
			int count) {
		if (transaction == null) {
			throw new IllegalArgumentException("Transaction cannot be null");
		}
		if (stock == null) {
			throw new IllegalArgumentException("Stock cannot be null");
		}

		return addStockToTransaction(transaction, stock, source, location, count, false);
	}

	private TransactionItem addStockToTransaction(Transaction transaction, Stock stockItem, Location source,
			Location location, int count, boolean exactLocation) {

		List<TransactionItem> existingItems = null;

		if (exactLocation && location != null) {
			existingItems = em.createNamedQuery("findExistingItemForStockAndLocation", TransactionItem.class)
					.setParameter("status", TransactionItemStatus.RESERVED).setParameter("stock", stockItem)
					.setParameter("transaction", transaction).setParameter("location", location).getResultList();

		} else {
			existingItems = em.createNamedQuery("findExistingItemForStock", TransactionItem.class)
					.setParameter("status", TransactionItemStatus.RESERVED).setParameter("stock", stockItem)
					.setParameter("transaction", transaction).getResultList();
		}

		TransactionItem inboundItem = null;
		if (existingItems.isEmpty()) {
			inboundItem = new TransactionItem();

			inboundItem.setSource(source);
			inboundItem.setCount(count);
			inboundItem.setTarget(location);

			inboundItem.setStock(stockItem);

			// Assign to transaction
			inboundItem.setTransaction(transaction);
			transaction.getTransactionItems().add(inboundItem);

			transactionRepository.save(transaction);
		} else {
			inboundItem = existingItems.get(0);
			inboundItem.setCount(inboundItem.getCount() + count);

			if (location != null) {
				inboundItem.setTarget(location);
			}
		}

		return inboundItem;
	}

	@Override
	public TransactionItem getItem(long id) {
		List<TransactionItem> items = em
				.createQuery("from TransactionItem item join fetch item.transaction where item.transactionItemId = :id",
						TransactionItem.class)
				.setParameter("id", id).getResultList();
		if (items.isEmpty()) {
			return null;
		}

		return items.get(0);
	}

	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public TransactionItem completeItemVerification(final TransactionItem txItem) {
		final TransactionItem item = em.merge(txItem);

		final StockLocation sourceLoc = stockService.find(txItem.getStock(), txItem.getSource());
		if (sourceLoc != null) {
			int result = sourceLoc.getCount() - txItem.getCount();
			if (result < 0) {
				throw new StockLocationCountUnavailableException(sourceLoc);
			} else if (result == 0) {
				em.remove(sourceLoc);
			} else {
				sourceLoc.setCount(result);
			}

		}

		StockLocation tgtLoc = stockService.find(txItem.getStock(), txItem.getTarget());
		if (tgtLoc == null) {
			tgtLoc = stockService.create(txItem.getStock(), txItem.getTarget(), txItem.getCount());
		} else {
			tgtLoc.setCount(tgtLoc.getCount() + txItem.getCount());
		}

		item.setStatus(TransactionItemStatus.VERIFIED);

		return item;
	}

	@Override
	public void deleteItem(TransactionItem item) {
		em.remove(item);
	}

	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public TransactionItem splitItem(final TransactionItem txItem, final Location newLocation, final int newCount) {
		if(txItem.getTarget() != null && txItem.getTarget().getLocationId() == newLocation.getLocationId()) {
			// A split without a location difference will do nothing.
			return txItem;
		}
		
		int remaining = txItem.getCount() - newCount;
		if (remaining == 0) {
			return alterItem(txItem, newLocation, txItem.getCount());
		}

		txItem.setCount(remaining);
		em.merge(txItem);

		return addStockToTransaction(txItem.getTransaction(), txItem.getStock(), txItem.getSource(), newLocation,
				newCount, true);
	}

	@Override
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public TransactionItem alterItem(TransactionItem txItem, Location newLocation, int newCount) {
		if(newCount == 0) {
			deleteItem(txItem);
			return txItem;
		}

		List<TransactionItem> existingItems = em
				.createNamedQuery("findExistingItemForStockAndLocation", TransactionItem.class)
				.setParameter("status", TransactionItemStatus.RESERVED).setParameter("stock", txItem.getStock())
				.setParameter("transaction", txItem.getTransaction()).setParameter("location", newLocation)
				.getResultList();

		TransactionItem retItem = null;
		if (existingItems.isEmpty()) {
			txItem.setTarget(newLocation);
			txItem.setCount(newCount);
			retItem = txItem;
		} else {
			try {
				retItem = existingItems.stream()
						.filter(item -> item.getTransactionItemId() != txItem.getTransactionItemId()).findFirst().get();
				retItem.setCount(retItem.getCount() + newCount);
				deleteItem(txItem);
			} catch (NoSuchElementException ex) {
				txItem.setTarget(newLocation);
				txItem.setCount(newCount);
				retItem = txItem;
			}
		}

		return retItem;
	}

}
