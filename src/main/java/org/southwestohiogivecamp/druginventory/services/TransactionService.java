package org.southwestohiogivecamp.druginventory.services;

import java.util.Date;
import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.southwestohiogivecamp.druginventory.domain.Transaction;
import org.southwestohiogivecamp.druginventory.domain.TransactionItem;

public interface TransactionService {
    Transaction get(long id);

    Transaction get(final String name);

    List<Transaction> getAllActive();

    List<Transaction> getAll(final Date start, final Date end);

    Transaction create(final String description, final int sourceInvId, final int targetInvId);
    
    TransactionItem addInboundItem(final Transaction transaction, final Packaging packaging, final Location source,
    		final Date expirationDate, final Location location, final int count);

    TransactionItem addOutboundItem(final Transaction transaction, final Stock stock, final Location source,
    		final Location location, final int count);
    
    TransactionItem getItem(long id);
    
    TransactionItem completeItemVerification(final TransactionItem txItem);
    TransactionItem alterItem(final TransactionItem txItem, final Location newLocation, final int newCount);
    TransactionItem splitItem(final TransactionItem txItem, final Location newLocation, final int newCount);

    void deleteItem(final TransactionItem item);
}

