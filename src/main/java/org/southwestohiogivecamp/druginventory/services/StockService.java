package org.southwestohiogivecamp.druginventory.services;

import java.util.Date;
import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.southwestohiogivecamp.druginventory.domain.StockLocation;

public interface StockService {
	
	Stock findOrCreate(Packaging packaging, Date expirationDate);

	List<Stock> searchStock(String packaging, Inventory context, Date expDate, Location source);
	
	StockLocation find(Stock stock, Location location);
	StockLocation create(Stock stock, Location location, int count);
}
