package org.southwestohiogivecamp.druginventory.services;

import java.util.List;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;

public interface InventoryService {

	List<Inventory> getInventoriesByType(InventoryType type);

	Inventory findOne(int id);

	List<Inventory> findAll();

	void save(Inventory inventory);

}