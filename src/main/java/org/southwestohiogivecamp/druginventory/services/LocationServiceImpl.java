package org.southwestohiogivecamp.druginventory.services;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.southwestohiogivecamp.druginventory.domain.Location_;
import org.southwestohiogivecamp.druginventory.reportbuilding.BarcodeReportBuilder;
import org.southwestohiogivecamp.druginventory.reportbuilding.BarcodeReportItem;
import org.southwestohiogivecamp.druginventory.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationRepository locationRepository;

	@Autowired
	private BarcodeReportBuilder barcodeReportBuilder;

	@PersistenceContext
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.southwestohiogivecamp.druginventory.services.LocationService#
	 * createLocationBarcodeReport(org.southwestohiogivecamp.druginventory.domain.
	 * InventoryType, java.io.OutputStream)
	 */
	@Override
	public void createLocationBarcodeReport(InventoryType inventoryType, OutputStream outputStream) {
		List<Location> locations = locationRepository.findByType(inventoryType);
		List<BarcodeReportItem> barcodeReportItems = locations.stream().map(LocationBarcodeReportItem::new)
				.collect(Collectors.toList());

		barcodeReportBuilder.CreateReport(inventoryType.toString() + " Locations", barcodeReportItems, outputStream);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.southwestohiogivecamp.druginventory.services.LocationService#findOne(java
	 * .lang.Long)
	 */
	@Override
	public Location findOne(Long id) {
		return locationRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.southwestohiogivecamp.druginventory.services.LocationService#findAll()
	 */
	@Override
	public List<Location> findAll() {
		return locationRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.southwestohiogivecamp.druginventory.services.LocationService#save(org.
	 * southwestohiogivecamp.druginventory.domain.Location)
	 */
	@Override
	public void save(Location location) {
		locationRepository.save(location);
	}

	private class LocationBarcodeReportItem implements BarcodeReportItem {
		private String code;
		private String description;

		public LocationBarcodeReportItem(final Location location) {
			code = String.format("%d", location.getLocationId());
			description = String.format("%s/%s", location.getInventory().getName(), location.getName());
		}

		@Override
		public String getCode() {
			return code;
		}

		@Override
		public String getDescription() {
			return description;
		}
	}

	@Override
	public List<Location> searchLocation(Inventory context, String locationSearch) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Location> crit = cb.createQuery(Location.class);
		final Root<Location> loc = crit.from(Location.class);
		
		List<Predicate> ands = new ArrayList<>();

		ands.add(cb.equal(loc.get(Location_.inventory), context));

		if (isNotEmpty(locationSearch)) {
			if (isNumeric(locationSearch)) {
				try {
					ands.add(cb.equal(loc.get(Location_.locationId), Long.parseLong(locationSearch)));
				} catch (NumberFormatException ex) {
					// ignore
				}
			} else {
				ands.add(cb.or(
						cb.like(loc.get(Location_.name), String.format("%%%s%%", locationSearch)),
						cb.like(loc.get(Location_.description), String.format("%%%s%%", locationSearch))));
			}
		}

		return em.createQuery(crit.where(ands.toArray(new Predicate[0]))).getResultList();
	}
}
