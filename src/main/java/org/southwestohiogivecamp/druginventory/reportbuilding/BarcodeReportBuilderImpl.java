package org.southwestohiogivecamp.druginventory.reportbuilding;

import java.io.OutputStream;
import java.util.List;

import com.itextpdf.barcodes.Barcode128;
import com.itextpdf.barcodes.Barcode1D;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.layout.property.VerticalAlignment;

public class BarcodeReportBuilderImpl implements BarcodeReportBuilder {

	@Override
	public void CreateReport(final String title, final List<BarcodeReportItem> barcodeReportItems,
			final OutputStream outputStream) {
		PdfWriter writer = new PdfWriter(outputStream);
		PdfDocument pdf = new PdfDocument(writer);
		Document document = new Document(pdf);
		document.add(new Paragraph(title));

		UnitValue[] tableWidths = new UnitValue[] { UnitValue.createPercentValue(0.25f),
				UnitValue.createPercentValue(0.75f) };

		Table table = new Table(tableWidths);
		table.setBorder(Border.NO_BORDER);

		for (BarcodeReportItem barcodeReportItem : barcodeReportItems) {
			// Barcode1D barcode = new Barcode39(pdf);
			Barcode1D barcode = new Barcode128(pdf);

			barcode.setCode(barcodeReportItem.getCode());
			barcode.setBarHeight(80f);
			barcode.setX(1.0f);

			PdfFormXObject formXObject = barcode.createFormXObject(pdf);

			Cell c = new Cell();
			c.setBorder(Border.NO_BORDER);
			c.add(new Image(formXObject));
			table.addCell(c);

			c = new Cell();
			c.setBorder(Border.NO_BORDER);
			c.setVerticalAlignment(VerticalAlignment.MIDDLE);
			c.add(barcodeReportItem.getDescription());
			table.addCell(c);
		}

		document.add(table);
		document.close();
	}
}
