package org.southwestohiogivecamp.druginventory.reportbuilding;

import java.io.OutputStream;
import java.util.List;

public interface BarcodeReportBuilder {
    void CreateReport(final String title, final List<BarcodeReportItem> barcodeReportItems,
                      final OutputStream outputStream);
}

