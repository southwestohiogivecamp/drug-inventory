package org.southwestohiogivecamp.druginventory.reportbuilding;

public interface BarcodeReportItem {
    String getCode();
    String getDescription();
}
