package org.southwestohiogivecamp.druginventory.config;

import org.southwestohiogivecamp.druginventory.reportbuilding.BarcodeReportBuilder;
import org.southwestohiogivecamp.druginventory.reportbuilding.BarcodeReportBuilderImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {
	
	@Bean
	public BarcodeReportBuilder getBarcodeReportBuilder() {
		return new BarcodeReportBuilderImpl();
	}

}
