package org.southwestohiogivecamp.druginventory.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class TransactionReportItem {
	private String srcLocation;
	private String dstLocation;
	private String medicationName;
	private String source;
	private String destination;
	private Date date;
	private String brand;
	private String medicineType;
	private int quantity;
	private Date expiration;
	private String dosing;
	private String ndc;
	private String manufacturer;
	private BigDecimal wac;
	private InventoryType inventoryType;
	private long packId;

	public TransactionReportItem(String srcLocation, String dstLocation, String medicationName, String source,
			String destination, Date date, String brand, String medicineType, int quantity, Date expiration,
			String dosing, String ndc, String manufacturer, BigDecimal wac, InventoryType inventoryType, long packId) {
		this.srcLocation = srcLocation;
		this.dstLocation = dstLocation;
		this.medicationName = medicationName;
		this.source = source;
		this.destination = destination;
		this.date = date;
		this.brand = brand;
		this.medicineType = medicineType;
		this.quantity = quantity;
		this.expiration = expiration;
		this.dosing = dosing;
		this.ndc = ndc;
		this.manufacturer = manufacturer;
		this.wac = wac;
		this.inventoryType = inventoryType;
		this.packId = packId;
	}

	public TransactionReportItem(String srcLocation, String dstLocation, String medicationName, String source,
			String destination, String brand, String medicineType, int quantity, Date expiration, String dosing,
			String ndc, String manufacturer, BigDecimal wac, InventoryType inventoryType, long packId) {
		this(srcLocation, dstLocation, medicationName, source, destination, null, brand, medicineType, quantity,
				expiration, dosing, ndc, manufacturer, wac, inventoryType, packId);
	}

	public long getPackId() {
		return packId;
	}

	public String getSrcLocation() {
		return srcLocation;
	}

	public String getDstLocation() {
		return dstLocation;
	}

	public String getDestination() {
		return destination;
	}

	public BigDecimal getExtValue() {
		if (wac == null)
			return null;
		return wac.multiply(new BigDecimal(quantity)).setScale(2, RoundingMode.HALF_UP);
	}

	public String getMedicationName() {
		return medicationName;
	}

	public String getSource() {
		return source;
	}

	public Date getDate() {
		return date;
	}

	public String getBrand() {
		return brand;
	}

	public String getMedicineType() {
		return medicineType;
	}

	public int getQuantity() {
		return quantity;
	}

	public Date getExpiration() {
		return expiration;
	}

	public String getDosing() {
		return dosing;
	}

	public String getNdc() {
		return ndc;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public BigDecimal getWac() {
		return wac;
	}

	public InventoryType getInventoryType() {
		return inventoryType;
	}

	public String getLocationForType() {
		return getInventoryType() == InventoryType.INBOUND ? getDstLocation() : getSrcLocation();
	}
}
