package org.southwestohiogivecamp.druginventory.domain;

public enum TransactionStatus {
	PLANNING, INPROCESS, COMPLETE;
}
