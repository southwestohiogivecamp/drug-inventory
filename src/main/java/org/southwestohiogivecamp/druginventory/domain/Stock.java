package org.southwestohiogivecamp.druginventory.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
		@NamedQuery(name = "findByPackagingAndExpiration", query = "from Stock s where s.packaging = :packaging and s.expirationDate = :expDate"),
		@NamedQuery(name = "findByPackagingWithNullExpiration", query = "from Stock s where s.packaging = :packaging and s.expirationDate is null") })
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long stockId;

	@ManyToOne(optional = false)
	@JoinColumn(name = "packaging_id")
	@NotNull
	private Packaging packaging;

	@Temporal(TemporalType.DATE)
	private Date expirationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate = new Date();

	@OneToMany(mappedBy = "stock")
	private List<StockLocation> stockLocations;

	public long getStockId() {
		return stockId;
	}

	public void setStockId(long stockId) {
		this.stockId = stockId;
	}

	public Packaging getPackaging() {
		return packaging;
	}

	public void setPackaging(Packaging packaging) {
		this.packaging = packaging;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<StockLocation> getStockLocations() {
		return stockLocations;
	}

	public void setStockLocations(List<StockLocation> stockLocations) {
		this.stockLocations = stockLocations;
	}

}
