package org.southwestohiogivecamp.druginventory.domain;

import static org.apache.commons.lang3.StringUtils.*;

public final class NdcCode {

	private static final int MAX_LEN = 11;
	private final String ndcCode;

	private NdcCode(final String ndcCode) {
		this.ndcCode = ndcCode;
	}

	public String getNdcCode() {
		return this.ndcCode;
	}

	public static NdcCode codeFor(final String code) {
		if (isNotBlank(code)) {
			if (isNumeric(code) && code.length() <= MAX_LEN && code.length() >= 8) {
				return new NdcCode(leftPad(code, MAX_LEN, "0"));
			}

			final String[] parts = code.split("-");
			if (parts.length == 3 && parts[0].length() <= 5 && parts[1].length() <= 4 && parts[2].length() <= 2) {
				final StringBuilder codeBuilder = new StringBuilder() //
						.append(leftPad(parts[0], 5, "0")) //
						.append(leftPad(parts[1], 4, "0")) //
						.append(leftPad(parts[2], 2, "0"));

				return new NdcCode(codeBuilder.toString());
			}
		}

		return null;
	}

}
