package org.southwestohiogivecamp.druginventory.domain;

public enum TransactionItemStatus {
	RESERVED, VERIFIED;
}
