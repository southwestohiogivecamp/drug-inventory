package org.southwestohiogivecamp.druginventory.domain.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static org.apache.commons.lang3.StringUtils.*;

public class NdcCodeValidator implements ConstraintValidator<NdcCode, String> {

	@Override
	public void initialize(NdcCode constraintAnnotation) {
		// nop;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// Don't validate an empty value.
		if (isEmpty(value)) {
			return true;
		}
		return org.southwestohiogivecamp.druginventory.domain.NdcCode.codeFor(value) != null;
	}

}
