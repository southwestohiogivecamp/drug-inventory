package org.southwestohiogivecamp.druginventory.domain;

public enum TransactionType {
	INBOUND, OUTBOUND, TRANSFER, RETURN;
}
