package org.southwestohiogivecamp.druginventory.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class StockLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long stockLocationId;

	@ManyToOne(optional = false)
	@JoinColumn(name = "stock_id")
	@NotNull
	private Stock stock;

	@ManyToOne(optional = false)
	@JoinColumn(name = "location_id")
	@NotNull
	private Location location;

	@Column(nullable = false)
	private int count = 0;

	public long getStockLocationId() {
		return stockLocationId;
	}

	public void setStockLocationId(long stockLocationId) {
		this.stockLocationId = stockLocationId;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
