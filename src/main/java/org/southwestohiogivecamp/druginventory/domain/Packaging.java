package org.southwestohiogivecamp.druginventory.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.southwestohiogivecamp.druginventory.domain.constraint.NdcCode;

@Entity
public class Packaging {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long packagingId;

	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;

	@Column(length = 4000, nullable = false)
	@NotNull
	@Size(max = 4000)
	private String description;

	@Size(max = 255)
	private String manufacturer;

	@Min(0)
	private BigDecimal wholesaleAverageCost;

	@Column(unique = true, length = 11)
	@NdcCode
	private String ndc;

	@Column(unique = true, length = 12)
	@Size(min = 12, max = 12)
	private String upc;

	@OneToMany(mappedBy = "packaging")
	private List<Stock> stocks;

	public List<Stock> getStocks() {
		return stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

	public long getPackagingId() {
		return packagingId;
	}

	public void setPackagingId(long packageId) {
		this.packagingId = packageId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNdc() {
		return ndc;
	}

	public void setNdc(String ndc) {
		this.ndc = ndc;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public BigDecimal getWholesaleAverageCost() {
		return wholesaleAverageCost;
	}

	public void setWholesaleAverageCost(BigDecimal wholesaleAverageCost) {
		this.wholesaleAverageCost = wholesaleAverageCost;
	}

}
