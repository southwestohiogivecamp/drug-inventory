package org.southwestohiogivecamp.druginventory.domain;

public enum InventoryType {
    INBOUND, OUTBOUND, INTERNAL;
}
