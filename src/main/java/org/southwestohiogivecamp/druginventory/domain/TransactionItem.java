package org.southwestohiogivecamp.druginventory.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = "findExistingItemForStock", query = "from TransactionItem item where item.status=:status and item.transaction = :transaction and item.stock = :stock"),
		@NamedQuery(name = "findExistingItemForStockAndLocation", query = "from TransactionItem item where item.status=:status and item.transaction = :transaction and item.stock = :stock and item.target = :location") })
public class TransactionItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long transactionItemId;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "stock_id")
	private Stock stock;
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "source_location_id")
	private Location source;
	@ManyToOne(optional = true)
	@JoinColumn(name = "target_location_id")
	private Location target;
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 10)
	private TransactionItemStatus status = TransactionItemStatus.RESERVED;
	@ManyToOne(optional = false)
	@JoinColumn(name = "transaction_id")
	private Transaction transaction;
	@Column(nullable = false)
	private int count = 0;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public long getTransactionItemId() {
		return transactionItemId;
	}

	public void setTransactionItemId(long transactionItemId) {
		this.transactionItemId = transactionItemId;
	}

	public Location getTarget() {
		return target;
	}

	public void setTarget(Location target) {
		this.target = target;
	}

	public TransactionItemStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionItemStatus status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Location getSource() {
		return source;
	}

	public void setSource(Location source) {
		this.source = source;
	}
}
