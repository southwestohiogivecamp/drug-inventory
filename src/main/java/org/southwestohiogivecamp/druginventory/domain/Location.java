package org.southwestohiogivecamp.druginventory.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Location {

	public Location() {
	}

	public Location(long locationId, String name, String description, Inventory inventory) {
		this.locationId = locationId;
		this.name = name;
		this.description = description;
		this.inventory = inventory;
	}

	public Location(String name, String description, Inventory inventory) {
		this.name = name;
		this.description = description;
		this.inventory = inventory;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long locationId;

	@NotNull
	@Size(min = 2, max = 255)
	@Column(unique = true, nullable = false)
	private String name;

	@Column(length = 4000)
	@Size(max = 4000)
	private String description;

	@ManyToOne(optional = false)
	@JoinColumn(name = "inventory_id")
	@NotNull
	private Inventory inventory;

	@Column(nullable = false)
	private boolean active = true;

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
