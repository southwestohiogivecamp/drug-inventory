package org.southwestohiogivecamp.druginventory.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long productId;
	@Column(nullable = false)
	@NotNull
	@Size(min = 2, max = 255)
	private String name;
	@Size(max = 255)
	private String brandName;
	@Size(max = 255)
	private String activeUnitStrength;
	@Size(max = 255)
	private String dosage;
	@Size(max = 255)
	private String route;
	@Size(max = 255)
	private String pharmacyClasses;
	@Size(max = 255)
	private String deaSchedule;
	@OneToMany(mappedBy = "product", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<Packaging> packages = new ArrayList<>();

	public List<Packaging> getPackages() {
		return packages;
	}

	public void setPackages(List<Packaging> packages) {
		this.packages = packages;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getActiveUnitStrength() {
		return activeUnitStrength;
	}

	public void setActiveUnitStrength(String activeUnitStrength) {
		this.activeUnitStrength = activeUnitStrength;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getPharmacyClasses() {
		return pharmacyClasses;
	}

	public void setPharmacyClasses(String pharmacyClasses) {
		this.pharmacyClasses = pharmacyClasses;
	}

	public String getDeaSchedule() {
		return deaSchedule;
	}

	public void setDeaSchedule(String deaSchedule) {
		this.deaSchedule = deaSchedule;
	}
}
