package org.southwestohiogivecamp.druginventory.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SqlResultSetMapping(name = "TransactionReportItem", classes = {
		@ConstructorResult(targetClass = TransactionReportItem.class, columns = { @ColumnResult(name = "srcLocation"),
				@ColumnResult(name = "dstLocation"), @ColumnResult(name = "medicationName"),
				@ColumnResult(name = "source"), @ColumnResult(name = "destination"),
				@ColumnResult(name = "date", type = Date.class), @ColumnResult(name = "brand"),
				@ColumnResult(name = "medicineType"), @ColumnResult(name = "quantity", type = Integer.class),
				@ColumnResult(name = "expiration", type = Date.class), @ColumnResult(name = "dosing"),
				@ColumnResult(name = "ndc"), @ColumnResult(name = "manufacturer"),
				@ColumnResult(name = "wac", type = BigDecimal.class),
				@ColumnResult(name = "inventoryType", type = Integer.class),
				@ColumnResult(name = "pack_id", type = Long.class) }) })

@Entity
public class Transaction {

	public Transaction() {
	}

	public Transaction(long transactionId, String description, Inventory source, Inventory destination, Date created,
			Date lastUpdated) {
		this.transactionId = transactionId;
		this.description = description;
		this.source = source;
		this.destination = destination;
		this.created = created;
		this.lastUpdated = lastUpdated;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long transactionId;

	@Column(nullable = false)
	private String description;

	@ManyToOne(optional = false)
	@JoinColumn(name="source_inventory_id")
	private Inventory source;

	@ManyToOne(optional = false)
	@JoinColumn(name="destination_inventory_id")
	private Inventory destination;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length=10)
	private TransactionStatus status = TransactionStatus.PLANNING;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date lastUpdated;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "transaction")
	private List<TransactionItem> transactionItems = new ArrayList<TransactionItem>();

	public List<TransactionItem> getTransactionItems() {
		return transactionItems;
	}

	public void setTransactionItems(List<TransactionItem> transactionItems) {
		this.transactionItems = transactionItems;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Inventory getSource() {
		return source;
	}

	public void setSource(Inventory source) {
		this.source = source;
	}

	public Inventory getDestination() {
		return destination;
	}

	public void setDestination(Inventory destination) {
		this.destination = destination;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	public TransactionType getTransactionType() {
		if (getSource().getInventoryType() == InventoryType.INBOUND) {
			return TransactionType.INBOUND;
		}
		if (getSource().getInventoryType() == InventoryType.OUTBOUND) {
			return TransactionType.RETURN;
		}
		if (getDestination().getInventoryType() == InventoryType.OUTBOUND) {
			return TransactionType.OUTBOUND;
		}

		if (getDestination().getInventoryType() == InventoryType.INBOUND) {
			throw new IllegalStateException("Cannot create a transaction with an 'INBOUND' destination");
		}

		return TransactionType.TRANSFER;
	}
}
