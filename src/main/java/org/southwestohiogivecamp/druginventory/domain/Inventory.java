package org.southwestohiogivecamp.druginventory.domain;

import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Inventory {

	public Inventory() {
	}

	public Inventory(String name, InventoryType inventoryType, String description) {
		this(0, name, inventoryType, description);
	}

	public Inventory(int inventoryId, String name, InventoryType inventoryType, String description) {
		this.inventoryId = inventoryId;
		this.name = name;
		this.inventoryType = inventoryType;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int inventoryId;

	@Column(nullable = false, unique = true)
	@Size(min = 2, max = 255)
	@NotNull
	private String name;

	@NotNull
	@Column(nullable = false)
	@Enumerated
	private InventoryType inventoryType;

	@Column(length = 4000)
	@Size(max = 4000)
	private String description;

	@OneToMany(mappedBy = "inventory")
	private List<Location> locations = Collections.emptyList();

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public int getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(int inventoryId) {
		this.inventoryId = inventoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InventoryType getInventoryType() {
		return inventoryType;
	}

	public void setInventoryType(InventoryType inventoryType) {
		this.inventoryType = inventoryType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
