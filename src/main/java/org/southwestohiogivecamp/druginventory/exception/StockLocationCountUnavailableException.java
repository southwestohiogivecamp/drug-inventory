package org.southwestohiogivecamp.druginventory.exception;

import org.southwestohiogivecamp.druginventory.domain.StockLocation;

public class StockLocationCountUnavailableException extends DrugInventoryException {

	private static final long serialVersionUID = -2501699917351883680L;

	private StockLocation stockLocation;

	public StockLocationCountUnavailableException(StockLocation stockLoc) {
		this.stockLocation = stockLoc;
	}
	
	public StockLocation getStockLocation() {
		return this.stockLocation;
	}

}
