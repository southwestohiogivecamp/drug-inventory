package org.southwestohiogivecamp.druginventory.exception;

public class DrugInventoryException extends RuntimeException {

	private static final long serialVersionUID = 3965829994896458745L;

	public DrugInventoryException() {
		super();
	}

	public DrugInventoryException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DrugInventoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public DrugInventoryException(String message) {
		super(message);
	}

	public DrugInventoryException(Throwable cause) {
		super(cause);
	}
	
}
