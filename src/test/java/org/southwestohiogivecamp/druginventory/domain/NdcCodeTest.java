package org.southwestohiogivecamp.druginventory.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class NdcCodeTest {
	
	private void shouldBeNull(String code, String message) {
		final NdcCode ndc = NdcCode.codeFor(code);
		assertNull(message, ndc);
	}
	
	private void goodCode(String code, String ndcCode) {
		final NdcCode ndc = NdcCode.codeFor(code);
		assertEquals(ndcCode, ndc.getNdcCode());
	}

	@Test
	public void allBadCodes() {
		shouldBeNull("123", "too short");
		shouldBeNull("", "it's blank");
		shouldBeNull(null, "Null value");
		shouldBeNull("some string", "Is not numeric");
		shouldBeNull("928374615243", "too long");
		shouldBeNull("012-1234567", "Bad split");
		shouldBeNull("998351-012-12", "First part too long");
		shouldBeNull("65243-65541-1", "Second part too long");
		shouldBeNull("77625-992-112", "Third part too long");
	}
	
	@Test
	public void allGoodCodes() {
		goodCode("19928765", "00019928765");
		goodCode("887266355", "00887266355");
		goodCode("2287639045", "02287639045");
		goodCode("09115243674", "09115243674");
		goodCode("1762-1162-2", "01762116202");
		goodCode("9-6-4", "00009000604");
	}

}
