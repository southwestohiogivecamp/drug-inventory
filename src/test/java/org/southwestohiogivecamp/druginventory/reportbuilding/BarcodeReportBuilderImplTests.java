package org.southwestohiogivecamp.druginventory.reportbuilding;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BarcodeReportBuilderImplTests {
    @Test
    public void CreateReport_GivenSimpleNameValuePairs_ReturnsProperlyFormattedPdf() {
        BarcodeReportBuilder barcodeReportBuilder = new BarcodeReportBuilderImpl();
        List<BarcodeReportItem> barcodeReportItems = new ArrayList<>();

        barcodeReportItems.add(new FakeBarcodeReportItem("FAKE1", "This is fake yo"));
        barcodeReportItems.add(new FakeBarcodeReportItem("FAKE2", "This is fake too"));
        barcodeReportItems.add(new FakeBarcodeReportItem("FAKE3", "Yeah, this is fake too"));

        try(OutputStream os = new ByteArrayOutputStream()) {
            barcodeReportBuilder.CreateReport("Test", barcodeReportItems, os);
        } catch (Exception e) {
            Assert.fail();
        }
    }

    private class FakeBarcodeReportItem implements BarcodeReportItem{
        private String code;
        private String description;

        private FakeBarcodeReportItem(String code, String description) {
            this.code = code;
            this.description = description;
        }

        @Override
        public String getCode() {
            return code;
        }

        @Override
        public String getDescription() {
            return description;
        }
    }
}
