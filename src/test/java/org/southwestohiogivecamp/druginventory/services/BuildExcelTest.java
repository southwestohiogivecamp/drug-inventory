package org.southwestohiogivecamp.druginventory.services;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BuildExcelTest {
	@Autowired
	ReportService reportService;

	@Test
	public void test() throws Exception {
		final Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -1);

		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			reportService.writeTransactionReport(baos, 1, c.getTime(), new Date());

			assertTrue("It appears that nothing was written!", baos.size() > 0);
		}
	}
}
