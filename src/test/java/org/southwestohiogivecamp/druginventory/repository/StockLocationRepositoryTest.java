package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class StockLocationRepositoryTest {
    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private StockLocationRepository stockLocationRepository;

    @Autowired
    private LocationRepository locationRepository;
    
    @Autowired
    private PackagingRepository pkgRepository;
    
    @Autowired
    private InventoryRepository invRepository;

    @Test
    public void testCreate() {
        Product prod = new Product();
        prod.setName("Name");

        Packaging pack = new Packaging();
        pack.setDescription("desc");
        pack.setNdc("01234567980");
        pack.setUpc("123456789012");
        pack.setProduct(prod);
        pkgRepository.save(pack);

        Stock stock = new Stock();
        stock.setPackaging(pack);
        stock.setExpirationDate(new Date());
        stockRepository.save(stock);

        Inventory inv = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        invRepository.save(inv);
        Location loc = new Location("Cincinnati", "asdf", inv);
        locationRepository.save(loc);

        StockLocation stockLoc = new StockLocation();
        stockLoc.setCount(7);
        stockLoc.setStock(stock);
        stockLoc.setLocation(loc);

        stockLocationRepository.save(stockLoc);

        long stockLocId = stockLoc.getStockLocationId();
        long stockId = stock.getStockId();
        long locId = loc.getLocationId();

        StockLocation savedStockLoc = stockLocationRepository.findOne(stockLocId);
        assertEquals(stockLoc.getCount(), savedStockLoc.getCount());

        Stock savedStock = savedStockLoc.getStock();
        assertNotNull(savedStock);
        assertEquals(stock.getExpirationDate(), savedStock.getExpirationDate());

        Location savedLoc = savedStockLoc.getLocation();
        assertNotNull(savedLoc);
        assertEquals(loc.getDescription(), savedLoc.getDescription());
        assertEquals(loc.getName(), savedLoc.getName());

        stockLocationRepository.delete(stockLocId);

        assertNull(stockLocationRepository.findOne(stockLocId));
        assertNotNull(stockRepository.findOne(stockId));
        assertNotNull(locationRepository.findOne(locId));
    }

    @Test
    public void testCreate_NoStock() {
        try {
            Inventory inv = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Location loc = new Location("Cincinnati", "asdf", inv);

            StockLocation stockLoc = new StockLocation();
            stockLoc.setCount(7);
            stockLoc.setStock(null);
            stockLoc.setLocation(loc);

            stockLocationRepository.save(stockLoc);
            fail();
        } catch (Exception e) {
            //success
        }

    }

    @Test
    public void testCreate_NoLocation() {
        try {
            Product prod = new Product();
            prod.setName("Name");

            Packaging pack = new Packaging();
            pack.setDescription("desc");
            pack.setNdc("01234567890");
            pack.setUpc("123456789012");
            pack.setProduct(prod);

            Stock stock = new Stock();
            stock.setPackaging(pack);
            stock.setExpirationDate(new Date());

            StockLocation stockLoc = new StockLocation();
            stockLoc.setCount(7);
            stockLoc.setStock(stock);
            stockLoc.setLocation(null);

            stockLocationRepository.save(stockLoc);
            fail();
        } catch (Exception e) {
            //success
        }
    }
}
