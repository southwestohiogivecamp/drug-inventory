package org.southwestohiogivecamp.druginventory.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.TransactionReportItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class ReportRepositoryTest {

    @Autowired
    private ReportRepository reportRepository;

    @Test
    public void testReport_TransactionReportItem() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -1);
        Date beginDate = c.getTime();
        Date endDate = new Date();
        List<TransactionReportItem> items = reportRepository.getTransactionReportItems(1, beginDate, endDate);
        assertNotNull(items);
        assertEquals(0, items.size());
    }
}
