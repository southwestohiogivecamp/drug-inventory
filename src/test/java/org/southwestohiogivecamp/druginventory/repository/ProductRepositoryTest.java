package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class ProductRepositoryTest {
    @Autowired
    private PackagingRepository packagingRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testCreate() {
        Product prod = new Product();
        prod.setName("Name");

        Packaging pack = new Packaging();
        pack.setDescription("desc");
        pack.setNdc("01234567890");
        pack.setUpc("123456789012");
        pack.setProduct(prod);
        packagingRepository.save(pack);

        List<Packaging> packs = new ArrayList<>();
        packs.add(pack);
        prod.setPackages(packs);

        productRepository.save(prod);

        long packId = pack.getPackagingId();
        long prodId = prod.getProductId();

        Product savedProd = productRepository.findOne(prodId);
        assertNotNull(savedProd);
        assertEquals(prod.getName(), savedProd.getName());

        List<Packaging> savedPacks = savedProd.getPackages();
        assertNotNull(savedPacks);
        assertEquals(1, savedPacks.size());

        Packaging savedPack = savedPacks.get(0);
        assertEquals(pack.getDescription(), savedPack.getDescription());
        assertEquals(pack.getNdc(), savedPack.getNdc());
        assertEquals(pack.getUpc(), savedPack.getUpc());


        productRepository.delete(prodId);

        assertNull(productRepository.findOne(prodId));
        assertNotNull(packagingRepository.findOne(packId));
    }

    @Test
    public void testCreate_NoPackaging() {
        Product prod = new Product();
        prod.setName("Name");

        productRepository.save(prod);
        long prodId = prod.getProductId();
        Product savedProd = productRepository.findOne(prodId);

        assertNotNull(savedProd);
        assertEquals(prod.getName(), savedProd.getName());

        List<Packaging> savedPacks = savedProd.getPackages();
        assertNotNull(savedPacks);
        assertEquals(0, savedPacks.size());
    }

    @Test
    public void testCreate_NoName() {
        try {
            Product prod = new Product();
            prod.setRoute("oral");

            productRepository.save(prod);
            fail();
        } catch (Exception e) {
            //success
        }
    }
}
