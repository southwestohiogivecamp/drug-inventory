package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class TransactionRepositoryTest {
    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Test
    public void testCreateNew() {
        Inventory source = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        Inventory dest = new Inventory("Inv2", InventoryType.OUTBOUND, "Sup");
        inventoryRepository.save(source);
        inventoryRepository.save(dest);

        Transaction trans = new Transaction();
        trans.setSource(source);
        trans.setDestination(dest);
        trans.setDescription("Test");
        trans.setLastUpdated(new Date());
        trans.setCreated(new Date());

        transactionRepository.save(trans);

        long transId = trans.getTransactionId();
        int sourceId = source.getInventoryId();
        int destId = dest.getInventoryId();

        Transaction savedTrans = transactionRepository.findOne(transId);
        assertNotNull(savedTrans);
        assertEquals(trans.getDescription(), savedTrans.getDescription());
        assertEquals(trans.getLastUpdated(), savedTrans.getLastUpdated());
        assertEquals(trans.getCreated(), savedTrans.getCreated());

        Inventory savedSource = savedTrans.getSource();
        assertNotNull(savedSource);
        assertEquals(source.getName(), savedSource.getName());

        Inventory savedDest = savedTrans.getDestination();
        assertNotNull(savedDest);
        assertEquals(dest.getName(), savedDest.getName());

        transactionRepository.delete(transId);

        assertNull(transactionRepository.findOne(transId));
        assertNotNull(inventoryRepository.findOne(sourceId));
        assertNotNull(inventoryRepository.findOne(destId));
    }

    @Test
    public void testCreateNew_NoSource() {
        try {
            //Inventory source = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Inventory dest = new Inventory("Inv2", InventoryType.OUTBOUND, "Sup");

            Transaction trans = new Transaction();
            trans.setSource(null);
            trans.setDestination(dest);
            trans.setDescription("Test");
            trans.setLastUpdated(new Date());
            trans.setCreated(new Date());

            transactionRepository.save(trans);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreateNew_NoDest() {
        try {
            Inventory source = new Inventory("Inv1", InventoryType.INBOUND, "Sup");

            Transaction trans = new Transaction();
            trans.setSource(source);
            trans.setDestination(null);
            trans.setDescription("Test");
            trans.setLastUpdated(new Date());
            trans.setCreated(new Date());

            transactionRepository.save(trans);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreateNew_NoDesc() {
        try {
            Inventory source = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Inventory dest = new Inventory("Inv2", InventoryType.OUTBOUND, "Sup");

            Transaction trans = new Transaction();
            trans.setSource(source);
            trans.setDestination(dest);
            trans.setDescription(null);
            trans.setLastUpdated(new Date());
            trans.setCreated(new Date());

            transactionRepository.save(trans);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreateNew_NoUpdated() {
        try {
            Inventory source = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Inventory dest = new Inventory("Inv2", InventoryType.OUTBOUND, "Sup");

            Transaction trans = new Transaction();
            trans.setSource(source);
            trans.setDestination(dest);
            trans.setDescription("Test");
            trans.setLastUpdated(null);
            trans.setCreated(new Date());

            transactionRepository.save(trans);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreateNew_NoCreated() {
        try {
            Inventory source = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Inventory dest = new Inventory("Inv2", InventoryType.OUTBOUND, "Sup");

            Transaction trans = new Transaction();
            trans.setSource(source);
            trans.setDestination(dest);
            trans.setDescription("Test");
            trans.setLastUpdated(new Date());
            trans.setCreated(null);

            transactionRepository.save(trans);
            fail();
        } catch (Exception e) {
            //success
        }
    }
}
