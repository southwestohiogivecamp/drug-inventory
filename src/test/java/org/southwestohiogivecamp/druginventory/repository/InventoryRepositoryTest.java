package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryRepositoryTest {
    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    LocationRepository locationRepository;

    @Test
    @Transactional
    public void testCreateFindAndDeleteWithLocation() throws Exception {
        Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        inventoryRepository.save(inventory);

        Location loc = new Location("Cincinnati", "asdf", inventory);
        locationRepository.save(loc);

        List<Location> locs = new ArrayList<>();
        locs.add(loc);
        inventory.setLocations(locs);

        int id = inventory.getInventoryId();
        long locId = loc.getLocationId();

        Inventory resultInv = inventoryRepository.findOne(id);
        assertEquals(inventory.getName(), resultInv.getName());
        assertEquals(inventory.getDescription(), resultInv.getDescription());
        assertEquals(inventory.getInventoryType(), resultInv.getInventoryType());
        assertNotNull(resultInv.getInventoryId());
        assertNotEquals(0, resultInv.getInventoryId());

        List<Location> resultLocs = resultInv.getLocations();
        assertNotNull(resultLocs);
        assertEquals(1, resultLocs.size());

        Location resultLoc = resultLocs.get(0);
        assertEquals(loc.getName(), resultLoc.getName());
        assertEquals(loc.getDescription(), resultLoc.getDescription());
        assertNotNull(loc.getLocationId());
        assertNotEquals(0, loc.getLocationId());

        inventoryRepository.delete(id);

        assertNull(inventoryRepository.findOne(id));

        Location resultLocDeleted = locationRepository.findOne(locId);
        assertNotNull(resultLocDeleted);
    }

    @Test
    public void testCreateFindAndDelete() throws Exception {
        Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        inventoryRepository.save(inventory);

        int id = inventory.getInventoryId();

        Inventory resultInv = inventoryRepository.findOne(id);
        assertEquals(inventory.getName(), resultInv.getName());
        assertEquals(inventory.getDescription(), resultInv.getDescription());
        assertEquals(inventory.getInventoryType(), resultInv.getInventoryType());
        assertNotNull(resultInv.getInventoryId());
        assertNotEquals(0, resultInv.getInventoryId());

        inventoryRepository.delete(id);

        assertNull(inventoryRepository.findOne(id));
    }

    @Test
    public void testCreate_NoName() throws Exception {
        try {
            Inventory inventory = new Inventory(null, InventoryType.INBOUND, "Sup");
            inventoryRepository.saveAndFlush(inventory);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreate_NoType() throws Exception {
        try {
            Inventory inventory = new Inventory("Test", null, "Sup");
            inventoryRepository.saveAndFlush(inventory);
            fail();
        } catch (Exception e) {
            //success
        }
    }


}