package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class TransactionItemRepositoryTest {
    @Autowired
    private TransactionItemRepository transactionItemRepository;
    
    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private LocationRepository locationRepository;
    
    @Autowired
    private TransactionRepository txRepo;
    
    @Autowired
    private ProductRepository prodRepo;
    
    @Autowired
    private PackagingRepository pkgRepo;
    
    @Autowired
    private StockRepository stockRepo;

    @Test
    public void testCreate() {
        Product prod = new Product();
        prod.setName("Name");
        prodRepo.save(prod);

        Packaging pack = new Packaging();
        pack.setDescription("desc");
        pack.setNdc("01234567890");
        pack.setUpc("123456789012");
        pack.setProduct(prod);
        pkgRepo.save(pack);

        Stock stock = new Stock();
        stock.setPackaging(pack);
        stock.setExpirationDate(new Date());
        stockRepo.save(stock);

        Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        inventoryRepository.save(inventory);
        Location loc = new Location("Cincinnati", "asdf", inventory);
        locationRepository.save(loc);

        Inventory dest = new Inventory("Inv2", InventoryType.OUTBOUND, "Sup");
        inventoryRepository.save(dest);

        Transaction trans = new Transaction();
        trans.setSource(inventory);
        trans.setDestination(dest);
        trans.setDescription("Test");
        trans.setLastUpdated(new Date());
        trans.setCreated(new Date());
        txRepo.save(trans);

        TransactionItem transItem = new TransactionItem();
        transItem.setStock(stock);
        transItem.setSource(loc);
        transItem.setTransaction(trans);
        transItem.setStatus(TransactionItemStatus.RESERVED);

        transactionItemRepository.save(transItem);

        long locId = loc.getLocationId();
        long transItemId = transItem.getTransactionItemId();

        TransactionItem savedTransItem = transactionItemRepository.findOne(transItemId);

        Location savedStockLoc = savedTransItem.getSource();
        assertNotNull(savedStockLoc);

        transactionItemRepository.delete(transItemId);

        assertNull(transactionItemRepository.findOne(transItemId));
        assertNotNull(locationRepository.findOne(locId));
    }

    @Test
    public void testCreate_NoLocation() {
        try {
            Product prod = new Product();
            prod.setName("Name");

            Packaging pack = new Packaging();
            pack.setDescription("desc");
            pack.setNdc("12345678901");
            pack.setUpc("123456789012");
            pack.setProduct(prod);

            Stock stock = new Stock();
            stock.setPackaging(pack);
            stock.setExpirationDate(new Date());

            //Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            //Location loc = new Location("Cincinnati", "asdf", inventory);

            TransactionItem transItem = new TransactionItem();
            transItem.setStatus(TransactionItemStatus.RESERVED);

            transactionItemRepository.save(transItem);
            fail();
        } catch (Exception e) {
            //success
        }

    }

    @Test
    public void testCreate_NoStatus() {
        try {
            Product prod = new Product();
            prod.setName("Name");

            Packaging pack = new Packaging();
            pack.setDescription("desc");
            pack.setNdc("12345678901");
            pack.setUpc("123456789012");
            pack.setProduct(prod);

            Stock stock = new Stock();
            stock.setPackaging(pack);
            stock.setExpirationDate(new Date());

            Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Location loc = new Location("Cincinnati", "asdf", inventory);

            TransactionItem transItem = new TransactionItem();
            transItem.setSource(loc);
            transItem.setStock(stock);

            transactionItemRepository.save(transItem);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreate_NoTrans() {
        try {
            Product prod = new Product();
            prod.setName("Name");

            Packaging pack = new Packaging();
            pack.setDescription("desc");
            pack.setNdc("12345678901");
            pack.setUpc("123456789012");
            pack.setProduct(prod);

            Stock stock = new Stock();
            stock.setPackaging(pack);
            stock.setExpirationDate(new Date());

            //Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            //Location loc = new Location("Cincinnati", "asdf", inventory);

            TransactionItem transItem = new TransactionItem();
            transItem.setStock(stock);
            transItem.setTransaction(null);
            transItem.setStatus(TransactionItemStatus.RESERVED);

            transactionItemRepository.save(transItem);
            fail();
        } catch (Exception e) {
            //success
        }
    }


}
