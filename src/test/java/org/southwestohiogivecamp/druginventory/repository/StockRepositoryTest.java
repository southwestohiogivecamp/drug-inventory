package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.southwestohiogivecamp.druginventory.domain.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class StockRepositoryTest {
    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private PackagingRepository packagingRepository;

    @Test
    public void testCreate() {
        Product prod = new Product();
        prod.setName("Name");

        Packaging pack = new Packaging();
        pack.setDescription("desc");
        pack.setNdc("01234567890");
        pack.setUpc("123456789012");
        pack.setProduct(prod);
        packagingRepository.save(pack);

        Stock stock = new Stock();
        stock.setPackaging(pack);
        stock.setExpirationDate(new Date());

        stockRepository.save(stock);

        long stockId = stock.getStockId();
        long packId = pack.getPackagingId();

        Stock savedStock = stockRepository.findOne(stockId);
        assertEquals(stock.getExpirationDate(), savedStock.getExpirationDate());

        Packaging savedPack = savedStock.getPackaging();
        assertNotNull(savedPack);
        assertEquals(pack.getDescription(), savedPack.getDescription());
        assertEquals(pack.getNdc(), savedPack.getNdc());
        assertEquals(pack.getUpc(), savedPack.getUpc());

        stockRepository.delete(stockId);

        assertNull(stockRepository.findOne(stockId));
        assertNotNull(packagingRepository.findOne(packId));
    }

    @Test
    public void testCreate_NoPackaging() {
        try {
            Stock stock = new Stock();
            stock.setExpirationDate(new Date());

            stockRepository.save(stock);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreate_NoExpiration() {
        try {
            Product prod = new Product();
            prod.setName("Name");

            Packaging pack = new Packaging();
            pack.setDescription("desc");
            pack.setNdc("01234567890");
            pack.setUpc("210987654321");
            pack.setProduct(prod);

            Stock stock = new Stock();
            stock.setPackaging(pack);

            stockRepository.save(stock);
            fail();
        } catch (Exception e) {
            //success
        }
    }
}
