package org.southwestohiogivecamp.druginventory.repository;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.Packaging;
import org.southwestohiogivecamp.druginventory.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class PackagingRepositoryTest {
    @Autowired
    private PackagingRepository packagingRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testCreateWithNewProduct() {
        Product prod = new Product();
        prod.setName("Name");
        productRepository.save(prod);

        Packaging pack = new Packaging();
        pack.setDescription("desc");
        pack.setNdc("01234567890");
        pack.setUpc("123456789012");
        pack.setProduct(prod);

        packagingRepository.save(pack);

        long packId = pack.getPackagingId();
        long prodId = prod.getProductId();

        Packaging savedPack = packagingRepository.findOne(packId);
        assertEquals(pack.getDescription(), savedPack.getDescription());
        assertEquals(pack.getNdc(), savedPack.getNdc());
        assertEquals(pack.getUpc(), savedPack.getUpc());

        Product savedProd = savedPack.getProduct();
        assertNotNull(savedProd);
        assertEquals(prod.getName(), savedProd.getName());

        packagingRepository.delete(packId);

        assertNull(packagingRepository.findOne(packId));
        assertNotNull(productRepository.findOne(prodId));
    }

    @Test
    public void testCreateWithOldProduct() {
        Product prod = new Product();
        prod.setName("Name");

        productRepository.save(prod);
        long prodId = prod.getProductId();
        prod = productRepository.findOne(prodId);

        Packaging pack = new Packaging();
        pack.setDescription("desc");
        pack.setNdc("01234567890");
        pack.setUpc("123456789012");
        pack.setProduct(prod);

        packagingRepository.save(pack);

        long packId = pack.getPackagingId();

        Packaging savedPack = packagingRepository.findOne(packId);
        assertEquals(pack.getDescription(), savedPack.getDescription());
        assertEquals(pack.getNdc(), savedPack.getNdc());
        assertEquals(pack.getUpc(), savedPack.getUpc());
    }

    @Test
    public void testCreate_NoDesc() {
        try {
            Product prod = new Product();
            prod.setName("Name");

            Packaging pack = new Packaging();
            pack.setNdc("1234567890");
            pack.setUpc("123456789012");
            pack.setProduct(prod);

            packagingRepository.save(pack);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void testCreate_NoProduct() {
        try {
            Packaging pack = new Packaging();
            pack.setDescription("desc");
            pack.setNdc("ndc");
            pack.setUpc("upc");
            pack.setProduct(null);

            packagingRepository.save(pack);
            fail();
        } catch (Exception e) {
            //success
        }
    }
}
