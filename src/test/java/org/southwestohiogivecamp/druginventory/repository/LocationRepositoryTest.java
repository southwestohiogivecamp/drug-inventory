package org.southwestohiogivecamp.druginventory.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.southwestohiogivecamp.druginventory.domain.Inventory;
import org.southwestohiogivecamp.druginventory.domain.InventoryType;
import org.southwestohiogivecamp.druginventory.domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class LocationRepositoryTest {
    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    LocationRepository locationRepository;

    @Test
    public void testCreateFindAndDeleteWithNewInventory() throws Exception {
        Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        inventoryRepository.save(inventory);
        Location loc = new Location("Cincinnati", "asdf", inventory);
        locationRepository.save(loc);

        int invId = inventory.getInventoryId();
        long locId = loc.getLocationId();

        Location resultLoc = locationRepository.findOne(locId);
        assertEquals(loc.getName(), resultLoc.getName());
        assertEquals(loc.getDescription(), resultLoc.getDescription());
        assertNotNull(loc.getLocationId());
        assertNotEquals(0, loc.getLocationId());

        Inventory resultInv = resultLoc.getInventory();
        assertNotNull(resultInv);
        assertEquals(inventory.getName(), resultInv.getName());
        assertEquals(inventory.getDescription(), resultInv.getDescription());
        assertEquals(inventory.getInventoryType(), resultInv.getInventoryType());
        assertNotNull(resultInv.getInventoryId());
        assertNotEquals(0, resultInv.getInventoryId());

        locationRepository.delete(locId);

        assertNull(locationRepository.findOne(locId));

        Inventory resultInvDeleted = inventoryRepository.findOne(invId);
        assertNotNull(resultInvDeleted);
    }

    @Test
    public void testCreateFindAndDeleteWithOldInventory() throws Exception {
        Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
        inventoryRepository.save(inventory);

        int invId = inventory.getInventoryId();

        inventory = inventoryRepository.findOne(invId);

        Location loc = new Location("Cincinnati", "asdf", inventory);
        locationRepository.save(loc);

        long locId = loc.getLocationId();

        Location resultLoc = locationRepository.findOne(locId);
        assertEquals(loc.getName(), resultLoc.getName());
        assertEquals(loc.getDescription(), resultLoc.getDescription());
        assertNotNull(loc.getLocationId());
        assertNotEquals(0, loc.getLocationId());

        Inventory resultInv = resultLoc.getInventory();
        assertNotNull(resultInv);
        assertEquals(inventory.getName(), resultInv.getName());
        assertEquals(inventory.getDescription(), resultInv.getDescription());
        assertEquals(inventory.getInventoryType(), resultInv.getInventoryType());
        assertNotNull(resultInv.getInventoryId());
        assertNotEquals(0, resultInv.getInventoryId());
    }

    @Test
    public void createNew_NoName() {
        try {
            Inventory inventory = new Inventory("Inv1", InventoryType.INBOUND, "Sup");
            Location loc = new Location(null, "asdf", inventory);
            locationRepository.save(loc);
            fail();
        } catch (Exception e) {
            //success
        }
    }

    @Test
    public void createNew_NoInventory() {
        try {
            Location loc = new Location("Test", "asdf", null);
            locationRepository.save(loc);
            fail();
        } catch (Exception e) {
            //success
        }
    }

}