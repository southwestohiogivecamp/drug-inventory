package org.southwestohiogivecamp.druginventory.export;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.southwestohiogivecamp.druginventory.export.dto.ExportGroup;
import org.southwestohiogivecamp.druginventory.export.dto.ExportRow;
import org.southwestohiogivecamp.druginventory.export.dto.ExportRow.RowType;

public class ExportSpreadsheetTest {

	public static void main(String[] args) throws IOException {
		ExportGroup group = new ExportGroup();

		ExportRow inbound = new ExportRow();
		inbound.setRowType(RowType.INBOUND);
		inbound.setLocation("202 A-0");
		inbound.setMedicationName("Amoxicillin 125mg/5ml oral susp., 150ml");
		inbound.setLine("");
		inbound.setSource("Americares");
		inbound.setDate(createDate(2017, 1, 4));
		inbound.setBrandName("Amoxil Suspension");
		inbound.setTypeOfMedicine("Antiobiotic, oral");
		inbound.setQuantity(240);
		inbound.setUnitType("Bottle");
		inbound.setExpDate(createDate(2017, 5, 1));
		inbound.setConditions("");
		inbound.setDosing("");
		inbound.setVerify("");
		inbound.setNdc("67253-0146-47");
		inbound.setManufacturer("Dava");
		inbound.setWac(new BigDecimal("3.75"));
		inbound.setExtValue(new BigDecimal("900.00"));
		inbound.setBeginning(null);
		inbound.setIns(new BigDecimal("0.00"));
		inbound.setOuts(null);
		inbound.setEnding(null);
		group.addTransaction(inbound);

		ExportRow outbound1 = new ExportRow();
		outbound1.setRowType(RowType.OUTBOUND);
		outbound1.setLocation("202 A-0");
		outbound1.setMedicationName("Amoxicillin 125mg/5ml oral susp., 150ml");
		outbound1.setLine("");
		outbound1.setSource("Guat - Feb");
		outbound1.setDate(createDate(2017, 1, 23));
		outbound1.setBrandName("Amoxil Suspension");
		outbound1.setTypeOfMedicine("Antiobiotic, oral");
		outbound1.setQuantity(-20);
		outbound1.setUnitType("Bottle");
		outbound1.setExpDate(createDate(2017, 5, 1));
		outbound1.setConditions("");
		outbound1.setDosing("");
		outbound1.setVerify("");
		outbound1.setNdc("67253-0146-47");
		outbound1.setManufacturer("Dava");
		outbound1.setWac(new BigDecimal("3.75"));
		outbound1.setExtValue(new BigDecimal("-75.00"));
		outbound1.setBeginning(null);
		outbound1.setIns(null);
		outbound1.setOuts(new BigDecimal("-75.00"));
		outbound1.setEnding(null);
		group.addTransaction(outbound1);

		ExportRow outbound2 = new ExportRow();
		outbound2.setRowType(RowType.OUTBOUND);
		outbound2.setLocation("202 A-0");
		outbound2.setMedicationName("Amoxicillin 125mg/5ml oral susp., 150ml");
		outbound2.setLine("");
		outbound2.setSource("Stanken Mexico");
		outbound2.setDate(createDate(2017, 1, 30));
		outbound2.setBrandName("Amoxil Suspension");
		outbound2.setTypeOfMedicine("Antiobiotic, oral");
		outbound2.setQuantity(-48);
		outbound2.setUnitType("Bottle");
		outbound2.setExpDate(createDate(2017, 5, 1));
		outbound2.setConditions("");
		outbound2.setDosing("");
		outbound2.setVerify("");
		outbound2.setNdc("67253-0146-47");
		outbound2.setManufacturer("Dava");
		outbound2.setWac(new BigDecimal("3.75"));
		outbound2.setExtValue(new BigDecimal("-180.00"));
		outbound2.setBeginning(null);
		outbound2.setIns(null);
		outbound2.setOuts(new BigDecimal("-180.00"));
		outbound2.setEnding(null);
		group.addTransaction(outbound2);

		ExportRow total = new ExportRow();
		total.setLocation("202A0");
		total.setMedicationName("Amoxicillin 125mg/5ml oral susp., 150ml");
		total.setLine("TOT");
		total.setSource("");
		total.setDate(createDate(2017, 12, 31));
		total.setBrandName("Amoxil Suspension");
		total.setTypeOfMedicine("Antiobiotic, oral");
		total.setQuantity(0);
		total.setUnitType("Bottle");
		total.setExpDate(createDate(2017, 5, 1));
		total.setConditions("");
		total.setDosing("");
		total.setVerify("");
		total.setNdc("67253-0146-47");
		total.setManufacturer("Dava");
		total.setWac(new BigDecimal("3.75"));
		total.setExtValue(new BigDecimal("0.00"));
		total.setBeginning(null);
		total.setIns(null);
		total.setOuts(null);
		total.setEnding(new BigDecimal("0.00"));
		group.setTotal(total);

		try (OutputStream out = new FileOutputStream(new File("C:/Users/Trey/Desktop/test.xlsx"))) {
			new ExportSpreadsheetService().exportTransactionReport(Arrays.asList(group), out);
		}
	}

	private static Date createDate(int year, int month, int day) {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		return cal.getTime();
	}

}
